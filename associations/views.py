#-*- coding: utf-8

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy, reverse
from django.core.exceptions import PermissionDenied
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect, Http404
import tooltip

from datetime import datetime

from associations.models import Association, Member, Activity
from associations.forms import GeneralInformationForm, ActivityForm, \
    ConditionsAgreementForm
from events.models import Event, Registration
from events.forms import NewEventForm
from messaging.api import create_private_thread
from portal.models import Announcement
from eistiens_net import util


def associations_home(request):
    """
    Displays the list of student associations.

    Homepage for the associations module.

    :param request: request object
    """
    association_list = Association.objects.all().order_by('name')

    tooltip.content(request,
        title="""Découvrez les <b>associations</b> !""",
        text="""Ne restez pas dans votre coin et faites bouger votre école !
            Le catalogue des associations est là pour vous aider à trouver
            chaussure à votre pied. Que vous soyez sportif, musicos ou grand
            nerd honoraire de la loge Klingon, il y a une asso qui vous
            ressemble !""")
    tooltip.icon(request, 'bullhorn')

    return render_to_response(
        "associations_portal.html",
        {
            'request': request,
            'association_list': association_list
        }, context_instance=RequestContext(request))


def association_display(request, association_name):
    """
    Displays public information about an association.

    This view gives all public information about an association : its
    activities, organized events and members.

    :param request : request object
    :param association_name: association's name
    """

    association = get_object_or_404(Association, name=association_name)
    activities = Activity.objects.filter(association=association)
    events = Event.objects.filter(organizer=association,
                                  day__gte=datetime.today())

    current_year = association.signing_year  # util.get_current_year()

    members = Member.objects.filter(
        association=association,
        year=util.get_current_year()).order_by('status')

    if request.user.is_authenticated():
        if Member.objects.filter(
            user=request.user.id,
            association=association.id,
            year=current_year).exists():

            member = Member.objects.get(
                user=request.user.id,
                association=association.id, year=current_year)

        elif Member.objects.filter(
            user=request.user.id,
            association=association.id,
            year=util.get_current_year()).exists():

            member = Member.objects.get(
                user=request.user.id,
                association=association.id, year=util.get_current_year())

        else:
            member = None
    else:
        member = None

    return render_to_response(
        "association2.html",
        {
            'request': request,
            'association': association,
            'members': members,
            'member': member,
            'activities': activities,
            'events': events,
        })


# Admin pages
# The following views should only be accessible by connected board members.

@login_required
def admin_general_infos(request, association_name):
    """
    Edits association's general informations.

    :param request: request object
    :param association_name: name of the association to edit
    """

    association = get_object_or_404(Association, name=association_name)
    current_year = association.signing_year  # util.get_current_year()

    # The connected user must be a board member (status < 5)
    if not Member.objects.filter(
            user=request.user.id, association=association.id,
            year=current_year, status__lt=5).exists():
        raise PermissionDenied

    message = False  # TODO : use the message middleware for confirmations
    information_form = GeneralInformationForm(
        request.POST or None, request.FILES or None, instance=association)

    if information_form.is_valid():
        information_form.save()
        message = True

    return render_to_response(
        "admin_general_info.html",
        {
            'request': request,
            'association': association,
            'form': information_form,
            'message': message
        },
        context_instance=RequestContext(request))


@login_required
def admin_activities(request, association_name):
    """
    Edit and create new activities.

    :param request: request object
    :param association_name: name of the association to manage
    """

    association = get_object_or_404(Association, name=association_name)
    current_year = association.signing_year  # util.get_current_year()

    # The connected user must be a board member (status < 5)
    if not Member.objects.filter(
            user=request.user.id, association=association.id,
            year=current_year, status__lt=5).exists():
        raise PermissionDenied

    message = False  # TODO : use the message middleware for confirmations
    create_activity_form = ActivityForm(
        request.POST or None, request.FILES or None)

    if create_activity_form.is_valid():
        new_activity = create_activity_form.save(commit=False)
        new_activity.association = association
        create_activity_form.save()
        create_activity_form = ActivityForm()

        message = True

    return render_to_response(
        "admin_activities.html",
        {
            'request': request,
            'form': create_activity_form,
            'association': association,
            # 'active_forms': active_forms,
            'message': message
        },
        context_instance=RequestContext(request))


@login_required
def admin_activity_edit(request, association_name, activity_id):

    association = get_object_or_404(Association, name=association_name)
    current_year = association.signing_year  # util.get_current_year()

    # The connected user must be a board member (status < 5)
    if not Member.objects.filter(
            user=request.user.id, association=association.id,
            year=current_year, status__lt=5).exists():
        raise PermissionDenied

    activity = get_object_or_404(
        Activity, id=activity_id, association=association.id)

    message = False  # TODO : use the message middleware for confirmations
    activity_edition_form = ActivityForm(
        request.POST or None, request.FILES or None, instance=activity)

    if activity_edition_form.is_valid():
        activity_edition_form.save()
        message = True

    return render_to_response(
        "admin_activities_edit.html",
        {
            'request': request,
            'association': association,
            'activity': activity,
            'form': activity_edition_form,
            'message': message
        },
        context_instance=RequestContext(request))


@login_required
def admin_activity_delete(request, association_name, activity_id):
    """
    Deletes an association's activity if the user has the proper access.

    This page will 404 if the user is not allowed to perform this action or if
    no activity exists with the specified id. If the activity deletion was
    successful, the user will be redirected to the event management page.

    :param request: request object
    :param association_name: association name
    :param activity_id: numerical id of the activity to delete
    """
    association = get_object_or_404(Association, name=association_name)
    current_year = association.signing_year  # util.get_current_year()

    # The connected user must be a board member (status < 5)
    if not Member.objects.filter(
            user=request.user.id, association=association.id,
            year=current_year, status__lt=5).exists():
        raise PermissionDenied

    activity = get_object_or_404(
        Activity, id=activity_id, association=association.id)
    activity.delete()

    return HttpResponseRedirect(reverse_lazy(
        'associations.views.admin_activities',
        args=(association_name,)))


@login_required
def admin_events(request, association_name):

    association = get_object_or_404(Association, name=association_name)
    current_year = association.signing_year  # util.get_current_year()

    # The connected user must be a board member (status < 5)
    if not Member.objects.filter(
            user=request.user.id, association=association.id,
            year=current_year, status__lt=5).exists():
        raise PermissionDenied

    events = Event.objects.filter(organizer=association.id)

    upcoming_events = events.filter(
        day__gte=datetime.today()).order_by('day', 'start_time')

    past_events = events.filter(
        day__lt=datetime.today()).order_by('day', 'start_time')

    # TODO rename form var name
    form = NewEventForm(request.POST or None, request.FILES or None)
    active_forms = {}  # TODO remove unsused form sets

    if 'action' in request.POST and request.POST['action'] == "new_event":
        if form.is_valid():
            new_event = form.save(commit=False)
            new_event.organizer_id = association.id
            form.save()

            announcement = Announcement(
                title=u"Participez à l'événement « %s » !" % new_event.title,
                category=0,
                content=u"""L'association %s vous invite à participer à son
                        événement « <b>%s</b> », venez vous inscrire
                        <a href="%s">ici</a>.""" % (association.name, new_event.title,
                            reverse('events.views.display_event', args=(new_event.id,))))
            announcement.save()
        else:
            active_forms['new_event'] = True

    return render_to_response(
        "admin_events.html",
        {
            'request': request,
            'association': association,
            'upcoming_events': upcoming_events,
            'past_events': past_events,
            'active_forms': active_forms,
            'form': form
        },
        context_instance=RequestContext(request))


@login_required
def admin_events_edit(request, association_name, event_id, *args):
    
    association = get_object_or_404(Association, name=association_name)
    current_year = association.signing_year  # util.get_current_year()

    # The connected user must be a board member (status < 5)
    if not Member.objects.filter(
            user=request.user.id, association=association.id,
            year=current_year, status__lt=5).exists():
        raise PermissionDenied

    participants = Registration.objects.filter(event=event_id)
    paid = participants.filter(paid=True)
    unpaid = participants.filter(paid=False)

    active_forms = {}  # TODO remove unsused form sets

    if 'action' in request.POST and request.POST['action'] == "edit_event":
        form = NewEventForm(request.POST or None, request.FILES or None)
        event = get_object_or_404(
            Event, id=event_id, organizer=association.id)

        if form.is_valid():
            new_event = form.save(commit=False)
            new_event.id = event_id
            new_event.organizer_id = association.id
            form.save()
        else:
            active_forms['new_event'] = True

    else:
        event = get_object_or_404(
            Event, id=event_id, organizer=association.id)
        form = NewEventForm(instance=event)

    return render_to_response(
        "admin_events_edit.html",
        {
            'request': request,
            'association': association,
            'event': event,
            'participants': participants,
            'paid': paid,
            'unpaid': unpaid,
            'form': form,
            'active_forms': active_forms,
            'tab': args[0]['tab'] if args else False
        },
        context_instance=RequestContext(request))


@login_required
def admin_events_set_paid(request, association_name, event_id, member_id):

    association = get_object_or_404(Association, name=association_name)
    current_year = association.signing_year  # util.get_current_year()

    # The connected user must be a board member (status < 5)
    if not Member.objects.filter(
            user=request.user.id, association=association.id,
            year=current_year, status__lt=5).exists():
        raise PermissionDenied

    registration = get_object_or_404(
            Registration, event__id=event_id, user__id=member_id)

    registration.paid = not registration.paid
    registration.save()

    return admin_events_edit(request, association_name, event_id, {'tab': 'participants'})


@login_required
def admin_board(request, association_name):

    association = get_object_or_404(Association, name=association_name)
    current_year = association.signing_year  # util.get_current_year()

    # The connected user must be a board member (status < 5)
    if not Member.objects.filter(
            user=request.user.id, association=association.id,
            year=current_year, status__lt=5).exists():
        raise PermissionDenied

    return render_to_response(
        "admin_board.html",
        {
            'request': request,
            'association': association,
            'roles': Member.MEMBER_STATUS[1:-1]
        })


@login_required
def admin_board_add_member(request, association_name, member_id, role):

    association = get_object_or_404(Association, name=association_name)
    current_year = association.signing_year  # util.get_current_year()

    # The connected user must be a board member (status < 5)
    if not Member.objects.filter(
            user=request.user.id, association=association.id,
            year=current_year, status__lt=5).exists():
        raise PermissionDenied

    member = get_object_or_404(
        Member, user=member_id, association=association.id, year=util.get_current_year())

    try:
        role = int(role)

        # Only one president per association per year
        if role != 0:
            # If it is the next board being created
            if association.signing_year != util.get_current_year():
                user = get_object_or_404(User, id=member_id)
                new_member = Member(
                    user=user,
                    association=association,
                    year=association.signing_year,
                    status=role)
                new_member.save()
            else:
                member.status = role
                member.save()
    except:
        pass

    return HttpResponseRedirect(reverse_lazy(
        'associations.views.admin_board', args=(association.name,)))


@login_required
def admin_board_kick_member(request, association_name, member_id):

    association = get_object_or_404(Association, name=association_name)
    current_year = association.signing_year  # util.get_current_year()

    # The connected user must be a board member (status < 5)
    if not Member.objects.filter(
            user=request.user.id, association=association.id,
            year=current_year, status__lt=5).exists():
        raise PermissionDenied

    member = get_object_or_404(
        Member, user=member_id, association=association.id, year=current_year)

    message = False
    if member.user.id == request.user.id:
        message = -1
    else:
        member.status = member.get_basic_member_status()
        member.save()

    return render_to_response(
        "admin_board.html",
        {
            'request': request,
            'association': association,
            'message': message
        })


@login_required
def admin_members(request, association_name):

    association = get_object_or_404(Association, name=association_name)
    current_year = association.signing_year  # util.get_current_year()

    # The connected user must be a board member (status < 5)
    if not Member.objects.filter(
            user=request.user.id, association=association.id,
            year=current_year, status__lt=5).exists():
        raise PermissionDenied

    # TODO use this variable in the template
    # and only query if subscription > 0
    nb_paid_memberships = Member.objects.filter(
        association=association.id, year=current_year,
        subscription_paid=True).count()

    return render_to_response(
        "admin_members.html",
        {
            'request': request,
            'association': association,
            "nb_paid_memberships": nb_paid_memberships,
            'subscription_sum': nb_paid_memberships * association.subscription
        })


@login_required
def admin_members_kick_member(request, association_name, member_id):

    association = get_object_or_404(Association, name=association_name)
    current_year = association.signing_year  # util.get_current_year()

    # The connected user must be a board member (status < 5)
    if not Member.objects.filter(
            user=request.user.id, association=association.id,
            year=current_year, status__lt=5).exists():
        raise PermissionDenied

    member = get_object_or_404(
        Member, user=member_id, association=association.id, year=current_year)

    member.delete()

    return HttpResponseRedirect(reverse_lazy(
        'associations.views.admin_members',
        args=(association_name,)))


@login_required
def admin_members_set_subscription(request, association_name, member_id):

    association = get_object_or_404(Association, name=association_name)
    current_year = association.signing_year  # util.get_current_year()

    # The connected user must be a board member (status < 5)
    if not Member.objects.filter(
            user=request.user.id, association=association.id,
            year=current_year, status__lt=5).exists():
        raise PermissionDenied

    member = get_object_or_404(
        Member, user=member_id, association=association.id, year=util.get_current_year())

    member.subscription_paid = not member.subscription_paid
    member.save()

    return HttpResponseRedirect(reverse_lazy(
        'associations.views.admin_members',
        args=(association_name,)))


@login_required
def admin_send_message(request, association_name, recipients):

    association = get_object_or_404(Association, name=association_name)
    current_year = association.signing_year  # util.get_current_year()

    # The connected user must be a board member (status < 5)
    if not Member.objects.filter(
            user=request.user.id, association=association.id,
            year=current_year, status__lt=5).exists():
        raise PermissionDenied

    if recipients not in ('tous', 'cotisations_retard'):
        raise Http404

    if request.method == 'POST':
        # Query the users the message will be sent to
        if recipients == 'tous':
            to_members = list(association.members.all())
        else:
            to_members = list(association.members.filter(
                member__subscription_paid=False))

        create_private_thread(request.POST.get('subject', ''),
            request.user, to_members, request.POST.get('content', ''))
        
        return HttpResponseRedirect(reverse_lazy(
                'associations.views.admin_members',
                args=(association_name,)))

    return render_to_response(
        'admin_send_message.html',
        {
            'request': request,
            'association': association,
            'recipients': recipients
        }, context_instance=RequestContext(request))


@login_required
def admin_signing(request, association_name):
    
    association = get_object_or_404(Association, name=association_name)
    current_year = association.signing_year  # util.get_current_year()

    if not Member.objects.filter(
            user=request.user.id, association=association.id,
            year=current_year, status=0).exists():
        raise PermissionDenied

    signing_done = (association.signing_year != util.get_current_year())

    if not signing_done and request.method == 'POST':
        user_id = request.POST.get('user_id', '')

        try:
            association.signing_year = util.get_current_year() + 1
            association.save()

            user = User.objects.get(id=int(user_id, 10))
            member = Member(
                user=user,
                association=association,
                year=association.signing_year,
                status=0)
            member.save()

            return HttpResponseRedirect(reverse_lazy(
                'associations.views.association_display',
                args=(association_name,)))

        except:
            messages.error(request, u'Utilisateur invalide')

    return render_to_response(
        'admin_signing.html', {
            'request': request,
            'association': association,
            'signing_done': signing_done
        }, context_instance=RequestContext(request))


@login_required
def admin_help(request, association_name):

    association = get_object_or_404(Association, name=association_name)
    current_year = association.signing_year  # util.get_current_year()

    # The connected user must be a board member (status < 5)
    if not Member.objects.filter(
            user=request.user.id, association=association.id,
            year=current_year, status__lt=5).exists():
        raise PermissionDenied

    return render_to_response(
        "admin_help.html",
        {
            'request': request,
            'association': association
        })


@login_required
def membership_conditions(request, association_name):
    """
    Displays the terms and conditions to join an association and asks the user
    his agreement.

    Those conditions may concern registration fees or any kind of conditions
    the board has decided. A member can consult those conditions, but he should
    not be able to accept it twice (not making much sense, does it ?).

    :param request: request object
    :param association_name: association the user wants to join
    """

    association = get_object_or_404(Association, name=association_name)
    current_year = util.get_current_year()

    already_member = Member.objects.filter(
        user=request.user, association=association, year=current_year).exists()
    agreement_form = ConditionsAgreementForm(request.POST or None)

    if not already_member and agreement_form.is_valid():

        if agreement_form.cleaned_data['agreement'].upper() == "J'ACCEPTE":
            new_member = Member(
                user=request.user,
                association=association,
                year=current_year,
                status=10)
            new_member.save()

            return HttpResponseRedirect(reverse_lazy(
                'associations.views.association_display',
                args=(association_name,)))

    if already_member:
        tooltip.content(request,
            title="""Consultation des conditions d'adhésion à
                {{ association.name }}""",
            text="""En tant que membre de l'association
                {{ association.name }}, vous avez déjà accepté les conditions
                énoncées ci-dessous pour l'année en cours. Vous pouvez
                consulter cette page à loisir afin de savoir tout ce que
                l'association peut attendre de vous.""")
        tooltip.icon(request, 'question-circle')

    return render_to_response(
        "membership_conditions.html",
        {
            'request': request,
            'association': association,
            'already_member': already_member,
            'agreement_form': agreement_form,
        },
        context_instance=RequestContext(request))


@login_required
def register_member(request, association_name):
    """
    Adds a registered user to the list of an association's members.

    If the user is already a member of the association, this view does nothing.

    :param request: request object
    :param association_name: association in which the user becomes a member
    """

    association = get_object_or_404(Association, name=association_name)
    current_year = util.get_current_year()

    already_member = Member.objects.filter(
        user=request.user, association=association, year=current_year).exists()

    if not already_member:
        new_member = Member(
            user=request.user,
            association=association,
            year=current_year,
            status=10)
        new_member.save()

    return HttpResponseRedirect(reverse_lazy(
        'associations.views.association_display', args=(association_name,)))
