from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',

    url(r'^$', 'associations.views.associations_home'),

    url(r'^(?P<association_name>[\w\W]+)/$',
        'associations.views.association_display'),

    url(r'^(?P<association_name>[\w\W]+)/register$',
        'associations.views.register_member'),

    url(r'^(?P<association_name>[\w\W]+)/conditions$',
        'associations.views.membership_conditions'),

    # Admin pages
    # Those views must be only usable by logged board members.

    url(r'^(?P<association_name>[\w\W]+)/admin/infos$',
        'associations.views.admin_general_infos'),

    url(r'^(?P<association_name>[\w\W]+)/admin/activites$',
        'associations.views.admin_activities'),

    url(r'^(?P<association_name>[\w\W]+)/admin/activites/'
        '(?P<activity_id>\w+)/edit$',
        'associations.views.admin_activity_edit'),

    url(r'^(?P<association_name>[\w\W]+)/admin/activites/'
        '(?P<activity_id>\w+)/delete$',
        'associations.views.admin_activity_delete'),

    url(r'^(?P<association_name>[\w\W]+)/admin/evenements$',
        'associations.views.admin_events'),

    url(r'^(?P<association_name>[\w\W]+)/admin/evenements/'
        '(?P<event_id>\w+)/edit$',
        'associations.views.admin_events_edit'),

    url(r'^(?P<association_name>[\w\W]+)/admin/evenements/'
        '(?P<event_id>\w+)/paye/(?P<member_id>\w+)$',
        'associations.views.admin_events_set_paid'),

    url(r'^(?P<association_name>[\w\W]+)/admin/bureau$',
        'associations.views.admin_board'),

    url(r'^(?P<association_name>[\w\W]+)/admin/bureau/'
        '(?P<member_id>\w+)/ajouter/(?P<role>[\w\W]+)$',
        'associations.views.admin_board_add_member'),

    url(r'^(?P<association_name>[\w\W]+)/admin/bureau/'
        '(?P<member_id>\w+)/kick$',
        'associations.views.admin_board_kick_member'),

    url(r'^(?P<association_name>[\w\W]+)/admin/membres$',
        'associations.views.admin_members'),

    url(r'^(?P<association_name>[\w\W]+)/admin/membres/'
        'message/(?P<recipients>\w+)$',
        'associations.views.admin_send_message'),

    url(r'^(?P<association_name>[\w\W]+)/admin/membres/'
        '(?P<member_id>\w+)/kick$',
        'associations.views.admin_members_kick_member'),

    url(r'^(?P<association_name>[\w\W]+)/admin/membres/'
        '(?P<member_id>\w+)/cotisation$',
        'associations.views.admin_members_set_subscription'),

    url(r'^(?P<association_name>[\w\W]+)/admin/passation$',
        'associations.views.admin_signing'),

    url(r'^(?P<association_name>[\w\W]+)/admin/help$',
        'associations.views.admin_help'),
)
