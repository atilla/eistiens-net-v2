# coding: utf-8

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models

import markdown2

from rooms.models import Room
from eistiens_net import util

from jobs.api import sanitize


def not_negative(value):
    if value < 0:
        raise ValidationError(u'The value has to be positive.')


class Association(models.Model):
    """
    An association instance reprensent a real-life student organisation.

    It has several attributes like the cotisation fee, a short description, in
    which campus it is located, etc.
    """

    name = models.CharField("Nom de l'association", max_length=64)

    description = models.TextField(
        "Description brève du rôle de l'association", blank=True)

    markdown_description = models.TextField(
            "Description brève du rôle de l'association (markdown)")

    website = models.URLField(
        "Site web de l'association",
        blank=True, null=True)

    logo = models.ImageField(
        "Logo de l'association",
        upload_to='img/associations/logos/',
        default='img/associations/logo_default.png')

    cover_image = models.ImageField(
        "Cover image",
        upload_to='img/associations/covers/',
        default='img/associations/cover_default.png')

    cover_caption = models.CharField(
        "Cover caption",
        max_length=512, blank=True, null=True)

    CAMPUS = (
        (0, "Cergy"),
        (1, "Pau"),
    )
    campus = models.SmallIntegerField("Campus", choices=CAMPUS)

    room = models.ForeignKey(
        Room,
        verbose_name='Local', blank=True, null=True)

    subscription = models.PositiveIntegerField(
        "Coût de la cotisation (laisser zéro si aucune)",
        default=0)

    signing_year = models.PositiveIntegerField('Année du bureau actuel', default=util.get_current_year())

    members = models.ManyToManyField(User, through='Member')

    def get_activities(self):
        """
        Returns a query set with all the association's activities.
        """
        return Activity.objects.filter(association=self.id)

    def get_board_members(self):
        """
        Returns a query set containing all the current board members, ordered
        by rank (most important first).
        """

        return Member.objects.filter(
            association=self.id,
            year=self.signing_year,
            status__lt=5).order_by('status')

    def get_members(self):
        """
        Returns a query set containing all the current year's members that are
        not in the board.
        """

        # TODO add options to optionaly order this set (by activity, or name
        # for instance).
        if self.signing_year == util.get_current_year():
            return Member.objects.filter(
                association=self.id, year=util.get_current_year(), status__gt=4)

        return Member.objects.filter(
                association=self.id, year=util.get_current_year())

    def get_all_members(self):
        """
        Returns a list containing all the current year's members.
        """

        return Member.objects.filter(
            association=self.id,
            year=util.get_current_year()).order_by(
                'status', 'user__last_name', 'user__first_name')

    def get_all_members_id(self):
        """
        Returns a list containing the IDs of all the current year's members,
        board members and simple members alike.
        """

        return Member.objects.filter(
            association=self.id,
            year=util.get_current_year()).values_list('user', flat=True)

    def get_president(self):
        """
        Returns the member designated as the president of the association
        """

        return Member.objects.get(
            association=self.id,
            year=self.signing_year,
            status=0)

    def is_passation_done(self):
        return Member.objects.filter(
            association=self.id,
            year=util.get_current_year()+1, status=0).exists()

    def save(self, *args, **kwargs):
        self.description = markdown2.markdown(self.markdown_description)
        self.description = sanitize(self.description)
        super(Association, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name


class GalleryImage(models.Model):
    """
    Galleries are an integrated picture and photos hosting system.

    It could be a convenient place to store events photos or posters.
    """

    # TODO : this model class is not used anywhere yet
    association = models.ForeignKey(Association)

    image = models.ImageField(
        "Photo ou image", upload_to='associations/logos/')


class Activity(models.Model):
    """
    Activities are the "features" of the association : what members usually do,
    and so on.

    Activities are not to be mixed up with events. Events are punctual whereas
    actities are more on a regular basis. For instance, if the sports
    association organizes a training session each week, it is more an activity
    than an event.
    """

    association = models.ForeignKey(Association)

    title = models.CharField("Titre de l'activité", max_length=150)

    description = models.TextField("Description courte", blank=True)

    markdown_description = models.TextField("Description courte (markdown)")

    image = models.ImageField(
        "Image de miniature",
        upload_to='associations/activites/')

    recurrence = models.CharField(
        "Récurrence de l'évènement",
        max_length=150, blank=True, null=True)

    website = models.URLField("Site web de l'activité", blank=True, null=True)

    def save(self, *args, **kwargs):
        self.description = markdown2.markdown(self.markdown_description)
        self.description = sanitize(self.description)
        super(Activity, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.association.__unicode__() + " : " + self.title


class Achievement(models.Model):
    """
    Distinctions are badges awarded to active members.
    """

    # TODO : this model class is not used anywhere yet
    association = models.ForeignKey(Association)

    title = models.CharField("Titre de la distinction", max_length=50)

    description = models.TextField("Description de la distinction")

    image = models.ImageField(
        "Icône de la distinction",
        upload_to='associations/distinctions', blank=True, null=True)

    def __unicode__(self):
        return self.association + " : " + self.title


class Member(models.Model):
    """
    A member is a user that is taking part to an association's actities.

    Members may have different statuses. Some are board members and thus can
    access the administration panel for the assiciation. Each membership
    contains the year, so the system can keep track of former members.
    """

    user = models.ForeignKey(User, verbose_name="Identité du membre")

    association = models.ForeignKey(Association, verbose_name="Association")

    MEMBER_STATUS = (
        (0, "Président"),
        (1, "Vice Président"),
        (2, "Secrétaire"),
        (3, "Trésorier"),
        (4, "Membre du bureau"),
        # Status 5 to 9 are available for future use
        (10, "Membre")
    )
    status = models.SmallIntegerField(
        "Statut du Membre",
        choices=MEMBER_STATUS)

    year = models.IntegerField("Année du début de l'inscritpion")

    subscription_paid = models.BooleanField("cotisation payée", default=False)

    achievements = models.ManyToManyField(Achievement, blank=True, null=True)

    def get_basic_member_status(self):
        for k, status in Member.MEMBER_STATUS:
            if status == "Membre":
                return k

    def __unicode__(self):
        return str(self.user) + "  (" + self.get_status_display() + \
            " " + self.association.name + ")"
