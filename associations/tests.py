from django.test import TestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
# from django.db.models.query import QuerySet

from associations.models import Association, Member
from associations.forms import GeneralInformationForm

from eistiens_net.util import get_current_year
from rooms.models import Buildind, Room


class AssociationModelTest(TestCase):
    """
    Test cases for the module's model classes.
    """

    def setUp(self):
        """
        Set up a few variables for the tests.
        """
        self.test_user = User.objects.create_user(
            'foobar',
            'foo@bar.com',
            'usertest')

        self.test_association = Association.objects.create(
            name="Cutie Mark Crusader",
            description="yay",
            campus=0)

        self.test_membre = Member.objects.create(
            user=self.test_user,
            association=self.test_association,
            status=0,
            # (member for member in Member.MEMBER_STATUS
            # if member[1] == 'Member').next()[0],
            year=get_current_year(),
            subscription_paid=True
        )

        self.test_building = Buildind.objects.create(
            name="Building",
            abbreviation="B0",
            campus=0,
            nb_floors=4)

        self.test_room = Room.objects.create(
            name="REDRUM",
            floor=1,
            building=self.test_building)

    def test_new(self):
        """
        Test case for basic association objects data coherence.
        """
        a = Association.objects.create(
            name="Cutie Mark Crusader",
            description="yay",
            campus=0)

        self.assertTrue(a.id > 0)
        self.assertEqual(a.subscription, 0)
        self.assertEqual(a.campus, 0)
        # self.assertEqual(a.site_web)
        self.assertEqual(a.members.count(), 0)

        a.room = self.test_room
        self.assertEqual(a.room.id, self.test_room.id)

    def test_members(self):
        """
        Tests adding members and board members.
        """

        test_association = Association.objects.create(
            name="Cutie Mark Crusader",
            description="yay",
            campus=0)

        nb_test_members = 50
        test_members = []
        for i in range(nb_test_members):
            m = Member.objects.create(
                user=self.test_user,
                association=test_association,
                year=get_current_year(),
                status=10)
            test_members.append(m)

        nb_board_members = 5
        test_board_members = []
        for i in range(nb_board_members):
            m = Member.objects.create(
                user=self.test_user,
                association=test_association,
                year=get_current_year(),
                status=i)
            test_board_members.append(m)

        self.assertEqual(
            nb_test_members + nb_board_members,
            test_association.get_members().count()
            + test_association.get_board_members().count())

        self.assertEqual(
            nb_board_members,
            test_association.get_board_members().count())

    def test_association_display(self):
        '''
            Just a prototype for view testing, will be expanded
        '''
        response = self.client.get(reverse(
            'associations.views.association_display',
            args=(self.test_association.name,)))

        self.assertEquals(response.context['association'].description, 'yay')
        self.assertEquals(response.context['members'][0], self.test_membre)
        self.assertEquals(response.context['members'].__len__(), 1)


class AssociationFormsTest(TestCase):
    """
    Test cases for module's forms.
    """

    def test_new_form_cotisation(self):
        """
        Unit test for the subscription field in a form.
        """
        form_data = {
            'name': 'Cutie Mark Crusader',
            'description': 'foo',
            'campus': 0,
            'cotisation': -42  # This should fail
            }
        form = GeneralInformationForm(data=form_data)
        self.assertFalse(form.is_valid())

        form_data = {
            'name': 'Cutie Mark Crusader',
            'description': 'foo',
            'campus': 0,
            'subscription': 42  # This should not fail
            }
        form = GeneralInformationForm(data=form_data)
        self.assertTrue(form.is_valid())
