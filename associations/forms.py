# coding: utf-8

from django import forms

from associations.models import Association, Member, Activity


class GeneralInformationForm(forms.ModelForm):
    """
    ModelForm to edit an association's parameters and informations.
    """

    class Meta:
        model = Association
        fields = ['markdown_description', 'website', 'logo', 'cover_image', 'subscription']


class ActivityForm(forms.ModelForm):
    """
    ModelForm to create or edit an association's activity.
    """

    class Meta:
        model = Activity
        exclude = ['association', 'description']


class NewBoardMemberForm(forms.Form):
    """
    Form used to add a new member to the board.
    """

    name = forms.CharField()
    status = forms.ChoiceField(Member.MEMBER_STATUS[:4])


class ConditionsAgreementForm(forms.Form):
    """
    Agreement form for new association members.
    """

    agreement = forms.CharField()
