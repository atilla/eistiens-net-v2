# coding: utf-8
from django.contrib import admin
from associations.models import Association, Achievement, Member, Activity

admin.site.register(Association)
admin.site.register(Achievement)
admin.site.register(Member)
admin.site.register(Activity)
