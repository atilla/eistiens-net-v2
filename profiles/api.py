from django.views.decorators.cache import cache_page
from django.http import HttpResponse

from profiles.models import Profile

import json


@cache_page(60 * 60 * 6)
def users_query(request, first_name, last_name, promotion):
    """
    Returns the list of users that match a students search.

    :param request: request object
    :param first_name: the first name to match (can be a prefix)
    :param last_name: the last name to match (can be a prefix)
    :param promotion: the promotion in which users will be searched
    """
    kwargs = {}

    if first_name != '_':
        kwargs['user__first_name__istartswith'] = first_name

    if last_name != '_':
        kwargs['user__last_name__istartswith'] = last_name

    if promotion != '_':
        try:  # In case we have a shitty user
            kwargs['status'] = int(promotion)
        except:
            pass

    profiles = Profile.objects.filter(**kwargs).order_by(
        'user__first_name', 'user__last_name')

    response = [{
        'last_name': profile.user.last_name,
        'first_name': profile.user.first_name,
        'username': profile.user.username,
        'picture': profile.avatar.url
    } for profile in profiles]

    return HttpResponse(json.dumps(response), content_type='application/json')