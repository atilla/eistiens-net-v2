from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',

    url(r'^annuaire$', 'profiles.views.users_directory'),

    url(r'^(?P<username>\w+)/$', 'profiles.views.display_profile'),
    url(r'^(?P<username>\w+)/edit$', 'profiles.views.edit_profile'),

    url(r'^mes_assos$', 'profiles.views.my_associations'),
    url(r'^mes_evenements$', 'profiles.views.my_events'),
)
