#coding: utf-8

"""
Views functions for the profils module.

Those views mostly deal with profile consultation and management along with
user parameters and settings, but also the internal messaging system.
"""

from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.template import RequestContext
from django.contrib import messages

from profiles.models import Profile
from profiles.forms import ProfileForm
from events.models import Registration
from associations.models import Member
from profiles.api import users_query

import json


def profiles_home(request):
    """
    Home page for profile module.

    :param request: request object
    """
    return render_to_response(
        "work_in_progress.html",
        {
            'request': request
        })



def display_profile(request, username):
    """
    Displays a user's profile.

    Public page that displays basic and profile informations relative to a
    given user.

    :param request: request object
    :param username: login name og the user whom profile is displayed
    """

    user = get_object_or_404(User, username=username)
    user_profile = get_object_or_404(Profile, user=user.id)

    # user's associations
    memberships = Member.objects.filter(user=user.id).order_by(
        '-year', 'status')

    return render_to_response(
        "profile.html",
        {
            'request': request,
            'user': user,
            'profile': user_profile,
            'memberships': memberships
        })


@login_required
def edit_profile(request, username):
    """
    Displays a form to edit the current user's profile.

    This page should only be assessible to the profile's owner. If the form is
    valid, the profile is updated and the view redirects to the profile
    consultation view.

    :param request: request object
    :param username: login name of the user whom profile is to be edited
    """

    user = get_object_or_404(User, username=username)
    # The following line should reject any illegitimate accesses :
    if request.user != user:
        raise PermissionDenied

    user_profile = get_object_or_404(Profile, user=user.id)

    profile_edition_form = ProfileForm(
        request.POST or None,
        request.FILES or None,
        instance=user_profile)

    if profile_edition_form.is_valid():
        profile_edition_form.save()
        messages.success(request, u'Votre profil a bien été mis à jour.')

    elif request.POST:
        messages.error(request, u'Erreur lors de la modification du profil.')

    return render_to_response(
        "profile_edition.html",
        {
            'request': request,
            'profile_edition_form': profile_edition_form,
        },
        context_instance=RequestContext(request))


@login_required
def my_associations(request):
    """
    Displays connected user's association memberships.

    This view displays all active as well as previous years association
    memberships.

    :param request: request object
    """

    memberships = Member.objects.filter(user=request.user)
    memberships.order_by('-year', 'status')

    return render_to_response(
        "mes_associations.html",
        {
            'request': request,
            'memberships': memberships
        })


@login_required
def my_events(request):
    """
    Displays connected user's events.

    This view displays all events to which the connected user has registered.

    :param request: request object
    """

    registrations = Registration.objects.filter(
        user=request.user.id).order_by('-event__day')

    future_events = [r.event for r in registrations if not r.event.is_past_event()]
    past_events = [r.event for r in registrations if r.event.is_past_event()]

    return render_to_response(
        "mes_evenements.html",
        {
            'request': request,
            'future_events': future_events,
            'past_events': past_events
        })


def users_directory(request):
    """
    Displays the directory containing all users, search can be performed.

    Can also make a query to let a user search other users in the database.

    :param request: request object
    """

    users = []
    first_name, last_name, status = '_', '_', '_'
    message = False

    birthdays = [b for b in Profile.objects.all() if b.is_birthday()]  # PAS BO

    if request.method == 'POST':
        first_name = request.POST.get('first_name', '_')
        last_name = request.POST.get('last_name', '_')
        status = request.POST.get('status', '_')

        if len(first_name) == 0:
            first_name = '_'

        if len(last_name) == 0:
            last_name = '_'

        users = users_query(request, first_name, last_name, status)
        users = json.loads(users.content)

        if len(users) == 0:
            message = True

        if status.isdigit():
            status = int(status)


    return render_to_response(
        "annuaire.html",
        {
            'request': request,
            'statuses': Profile.STATUS,
            'birthdays': birthdays,
            'users': users,
            'first_name': first_name,
            'last_name': last_name,
            'status': status,
            'message': message
        }, context_instance=RequestContext(request))
