# coding: utf-8

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import models

from messaging.models import PrivateThread

from datetime import datetime


class Profile(models.Model):
    """
    A profile contains all additional information associated with a user.

    It is a convenient place to store any information about the site's users
    and that doesn't fit into the default User model, such as social network
    profiles, website adress or even a profile image.
    To access a profile from a user objet, simply use 'user.profile'.
    """

    user = models.OneToOneField(User, verbose_name="Utilisateur")

    STATUS = (
        (0, "Personnel EISTI"),
        (1, "Professeur"),
        # Status 2 to 4 are available for future use
        (5, "CPI 1"),
        (6, "CPI 2"),
        (7, "ING 1"),
        (8, "ING 2"),
        (9, "ING 3"),
        (10, "Master"),
        (11, "Ancien"),
    )
    status = models.SmallIntegerField(
        "Promotion ou fonction", choices=STATUS, blank=True, null=True)

    pseudo = models.CharField(
        "Pseudonyme", max_length=100, blank=True, null=True)

    avatar = models.ImageField(
        "Avatar de l'utilisateur",
        upload_to='img/users/avatars/',
        default='img/users/default_avatar_128.png')

    bio = models.TextField("Présentation", blank=True, null=True)

    birthdate = models.DateField(
        "Date de naissance", blank=True, null=True)

    # Social networks and websites
    linkedIN = models.URLField("URL profil LinkedIN", blank=True, null=True)

    viadeo = models.URLField("URL profil Viadeo", blank=True, null=True)

    twitter = models.URLField("URL profil Twitter", blank=True, null=True)

    facebook = models.URLField("URL profil Facebook", blank=True, null=True)

    google_plus = models.URLField("URL profil Google+", blank=True, null=True)

    website = models.URLField(
        "Site internet personnel", blank=True, null=True)

    # Last time the user checked his/her private messages
    last_private_messages_check = models.DateTimeField(
        'Derniere lecture des messages privés', auto_now_add=True)

    def __unicode__(self):
        return str(self.user) + " profile"

    def is_birthday(self):
        if self.birthdate is None:
            return False

        today = datetime.today()
        birth = self.birthdate

        return today.day == birth.day and today.month == birth.month

    def get_nb_messages(self):
        """
        Returns the number of received messages (archived or not) for a given
        user.
        """
        # return Message.objects.filter(user_to=self.user).count()
        return 0

    def get_nb_unread_messages(self):
        """
        Returns the number of unread/not archived messages for a given user.
        """
        return PrivateThread.objects.filter(participants=self.user,
            last_message_date__gt=self.last_private_messages_check).count()


@receiver(post_save, sender=User)
def new_user_handler(instance, created, **kwargs):
    """
    Event handler that creates a Profile each time a new user is created.

    This handler is called each time a user object is saved in the database.
    This could mean the user has been either created or updated. Thus a profile
    should only be created for new user objects.

    :param instance: newly created or modified user
    :param created: True if the user was created and not updated
    """

    if created and not instance.is_superuser:
        new_profile = Profile(user=instance)
        new_profile.save()
