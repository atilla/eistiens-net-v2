from django import forms
from profiles.models import Profile
# from profiles.models import Message


class ProfileForm(forms.ModelForm):
    """
    ModelForm to edit a user profile (Profil model class).

    This form only exculdes the user field, for a profile cannot be attributed
    to another user. See the Profil model class for more details.
    """

    class Meta:
        model = Profile
        exclude = ['user', 'last_private_messages_check']
