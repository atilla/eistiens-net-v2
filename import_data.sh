#! /bin/bash

apps=(auth.User associations events jobs messaging portal profiles rooms);

for app in "${apps[@]}"
do
	echo Importing "$app"...
        `python manage.py loaddata fixtures/"$app.json" > /dev/null`
done

