#! /bin/bash

apps=(auth.User associations events jobs messaging portal profiles rooms);i

for app in "${apps[@]}"
do
	echo Exporting "$app"...
	`python manage.py dumpdata "$app" --indent 4 -e contenttypes > fixtures/"$app.json"`
done
