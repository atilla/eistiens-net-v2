from django.contrib import admin
from facts.models import Fact, Rating

admin.site.register(Fact)
admin.site.register(Rating)
