# coding: utf-8

from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.decorators import login_required

from facts.models import Fact, Rating

@login_required
def facts_dates(request):
    """Displays all the facts, ordered by date of publication.

    """

    facts = Fact.objects.all().order_by('-pub_date')

    return render_to_response(
        "facts.html",
        {
            'request': request,
            'facts': facts
        })
    
@login_required
def facts_ratings(request):
    """Displays all the facts, ordered by rating.

    """

    facts = Fact.objects.all().order_by('date') # TODO group by likes (queryset), pass the numbers of likes/dislikes to the template

    return render_to_response(
        "facts.html",
        {
            'request': request,
            'facts': facts
        })
    
@login_required
def add_fact(request):
    """Allows to add a fact.

    """
    pass

    
