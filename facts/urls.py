from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',

    url(r'^$', 'facts.views.facts_dates'),

    url(r'^popularity/$', 'facts.views.facts_ratings'),

)
