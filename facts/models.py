from django.contrib.auth.models import User
from django.db import models

class Fact(models.Model):
    """A fact is either a quote, or a 'VDM', or anything that can fit and
    be rated by the populace.

    """

    author = models.ForeignKey(User, verbose_name="Auteur")

    pub_date = models.DateTimeField(auto_now=True, auto_now_add=True, verbose_name="Date de publication")

    FACT_TYPE = (
        (0, "Quote"),
        (1, "VDM")
    )
    fact_type = models.SmallIntegerField("Type de fact", choices=FACT_TYPE)

    content = models.TextField("Contenu")

    content_markdown = models.TextField("Contenu (markdown)")

    is_draft = models.BooleanField(default=True)


class Rating(models.Model):
    """A Rating encode a like/dislike by a user for a one fact.

    """

    fact = models.ForeignKey(Fact, verbose_name="Fact", null=False)

    user = models.ForeignKey(User, verbose_name="Utilisateur", null=False)

    RATING_TYPE = (
        (0, "Like"), 
        (1, "Dislike")
    )
    rating_like = models.SmallIntegerField("Type de rating", choices=RATING_TYPE)
    
