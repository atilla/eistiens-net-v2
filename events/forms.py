# coding: utf-8

from django import forms

from events.models import Event


class NewEventForm(forms.ModelForm):
    """
    ModelForm to create or edit an event organized by an association.

    The base model for this form is located in the evenements module.
    """

    class Meta:
        model = Event
        exclude = [
            'organizer',
            'authorized',
            'registration_closed',
            'participants']
