# coding: utf-8

"""
Views functions for the events module.

Those views deal with the consultation of informations relative to events, and
also user registrations to such events. Keep in mind the views relative to
event creation and edition are located inside the association module's views
for associations only can manage events.
"""


from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import RequestContext
import tooltip

import json
from datetime import datetime

from profiles.models import Profile
from events.models import Event, Registration
from events.api import create_event_description


def get_week_number(date):
    """
    Give the week number of a day.

    In the way the events are currently displayed, it is needed to know in
    which week of the year a given day is.
    For more information, see datetime.isocalendar() method.

    :param date: datetime object
    """

    # The second element of the list returned by isocalendar() is the week
    # number.
    return date.isocalendar()[1]


def get_week_boundaries(offset=0):
    """
    Returns the first and last day of a week.

    This function is used to sort events depending on which week they're
    planned. It returns the date of the first monday of a week up to the
    friday. The targeted week is <offset> weeks away from the current week.
    Thus, offset=0 will give the current week bounds, while offset=1 will give
    the next week ones.

    :param offset: number of weeks ahead, starting with the current week
    """

    monday_ordinal = 7*(datetime.now().toordinal()/7) + 1 + (offset*7)
    friday_ordinal = monday_ordinal + 4

    return (
        datetime.fromordinal(monday_ordinal),  # First day of the week
        datetime.fromordinal(friday_ordinal))  # Last day of the week


def agenda(request):
    """
    Displays a list of upcoming events.

    This views is the homepage of the events module. It shows a list of
    upcoming events, and splits them into three categories : current week
    event, next week, and further events.

    :param request: request object
    """

    today = datetime.today()
    two_weeks_later, _ = get_week_boundaries(2)

    # Querying all upcoming events
    upcoming_events = Event.objects.filter(
        day__gte=datetime.today(), day__lte=two_weeks_later)
    upcoming_events.order_by('day', 'start_time')

    later_events_count = Event.objects.filter(day__gt=two_weeks_later).count()

    tooltip.content(request,
        title="""Planning des événements""",
        text="""Ce serait dommage de passer à côté des activités proposées par
            les associations !""")
    tooltip.icon(request, 'calendar')
    tooltip.button(request,
        text='<i class="fa fa-calendar-o"></i>  Calendrier des événements',
        view='events.views.calendar')

    return render_to_response(
        "agenda2.html",
        {
            'request': request,
            'current_day': today,
            'end_two_week_span': two_weeks_later,
            'event_list': upcoming_events,
            'later_events_count': later_events_count,
        }, context_instance=RequestContext(request))

# For display panel
def agenda_panel(request):
    """
    Displays a list of upcoming events and birthdays.

    This views specialy use for the display panel like EisTV. It 
    shows a list of upcoming events, and splits them into three
    categories : current week event, next week, and further events.
    It shows a list of birthdays of the day.

    :param request: request object
    """
    today = datetime.today()
    two_weeks_later, _ = get_week_boundaries(2)

    # Querying all upcoming events
    upcoming_events = Event.objects.filter(
        day__gte=datetime.today(), day__lte=two_weeks_later)
    upcoming_events.order_by('day', 'start_time')

    later_events_count = Event.objects.filter(day__gt=two_weeks_later).count()

    birthdays = [b for b in Profile.objects.all() if b.is_birthday()]


    return render_to_response(
        "agenda_panel.html",
        {
            'request': request,
            'current_day': today,
            'end_two_week_span': two_weeks_later,
            'event_list': upcoming_events,
            'later_events_count': later_events_count,
            'birthdays': birthdays,

        }, context_instance=RequestContext(request))




def calendar(request):
    """
    Displays a calendar, with all the event within a given month.

    This view is shown when the user wants to see more events, future or
    past events.

    :param request: request object
    """

    return render_to_response(
        'calendrier.html',
        {
            'request': request
        })


def ajax_calendar(request):
    """
    Returns a JSON object to feed the FullCalendar via AJAX

    The request takes 'start' and 'end' as GET parameters, under a YYYY-MM-DD
    format.

    :param request: request object
    """

    if request.method != 'GET':
        return HttpResponse(json.dumps(
            {
                "success": 0,
                "error": "Not a GET request"
            }), content_type="application/json")

    try:
        start = request.GET['start']
        end = request.GET['end']
    except:
        return HttpResponse(json.dumps(
            {
                "success": 0,
                "error": "Need start and end in GET"
            }), content_type="application/json")

    # Parse the start and end dates to datetime objects
    start_date = datetime.strptime(start, '%Y-%m-%d').date()
    end_date = datetime.strptime(end, '%Y-%m-%d').date()

    events = Event.objects.filter(day__range=(start_date, end_date))

    result = []
    for event in events:
        result.append({
            'id': len(result),
            'title': event.title,
            'start': datetime.combine(event.day, event.start_time).isoformat(),
            'end': datetime.combine(event.day, event.end_time).isoformat(),
            'description': create_event_description(event),
            'color': 'rgb(102, 28, 53)',
            'editable': False
        })

    return HttpResponse(json.dumps(result), content_type="application/json")


def display_event(request, event_id):
    """
    Displays informations relative to an event.

    This views give all publicly available informations about a given event,
    such as the date, place, and participants.

    :param request: request object
    :param event_id: event unique numeric id
    """

    event = get_object_or_404(Event, id=event_id)

    participants = event.participants.all()
    nb_available_places = event.max_nb_participants-participants.count()

    if event.is_past_event():
        tooltip.content(request,
            title="""Cet évènement appartient au passé...""",
            text="""Cet évènement a déjà eu lieu et a été archivé : vous ne
                pouvez plus vous y inscrire, mais il reste consultable.""")
        tooltip.icon(request, 'clock-o')

    elif not event.registration_possible():
        tooltip.content(request,
            title="""Navré, mais il n'est plus possible de s'inscrire à cet
                évènement""",
            text="""Il se peut que l'organisateur ait décidé de suspendre les
                inscriptions ou bien il ne reste plus de places disponibles.
                Tentez de nouveau dans quelque temps, avec un peu de chance il
                se pourrait qu'une place se libère.""")
        tooltip.icon(request, 'frown')

    return render_to_response(
        "display_event.html",
        {
            'request': request,
            'event': event,
            'participants': participants,
            'nb_available_places': nb_available_places
        }, context_instance=RequestContext(request))


@login_required
def event_register(request, event_id):
    """
    Registers the connected user to an event.

    Adds the connected user to the list of participants in an event, thus login
    is required. Once the registration completed (wether successful or not),
    the user is redirected to the event's information page (display_event).

    :param request: request object
    :param event_id: event unique numeric id
    """

    event = get_object_or_404(Event, id=event_id)

    already_registered = Registration.objects.filter(
        user=request.user.id, event=event_id).exists()

    # To be eligible, the user must :
    #
    #   1) not being already registered
    #   2) AND either :
    #       - attending a public event (no max. number of participants)
    #       - or there are remaining places in a private event

    if not already_registered and event.registration_possible():
        registration = Registration(user=request.user, event=event)
        registration.save()

    return HttpResponseRedirect(reverse_lazy(
        'events.views.display_event',
        args=(event_id,)))


@login_required
def event_unregister(request, event_id):
    """
    Unregisters a user from an event.

    Removes the connected user from the list of participants in an event, thus
    login is required. If the user is not already registered, this view does
    nothing. Once the registration deleted, the user is redirected to the
    event's information page (display_event).

    :param request: request object
    :param event_id: event unique numeric id
    """

    event = get_object_or_404(Event, id=event_id)

    if Registration.objects.filter(
            user=request.user.id, event=event.id).exists():
        registration = Registration.objects.get(
            user=request.user.id, event=event.id)
        registration.delete()

    return HttpResponseRedirect(reverse_lazy(
        'events.views.display_event',
        args=(event.id,)))


@login_required
def events_validations(request):

    if request.user.username != 'administration':
        raise Http404

    unauthorized_events = Event.objects.filter(authorized=False).order_by(
        'day', 'start_time')

    authorized_events = Event.objects.filter(authorized=True).order_by(
        'day', 'start_time')

    return render_to_response(
        "events_validation.html",
        {
            'request': request,
            'unauthorized_events': unauthorized_events,
            'authorized_events': authorized_events
        })


@login_required
def validate_one(request, event_id):

    if request.user.username != 'administration':  # and request.user.username != 'raynaudfla':
        raise Http404

    event = get_object_or_404(Event, id=event_id)
    event.authorized = True
    event.save()

    return HttpResponseRedirect(reverse_lazy('events.views.events_validations'))


@login_required
def validate_all(request):

    if request.user.username != 'administration':  # and request.user.username != 'raynaudfla':
        raise Http404

    events = Event.objects.filter(authorized=False)
    for event in events:
        event.authorized = True
        event.save()

    return HttpResponseRedirect(reverse_lazy('events.views.events_validations'))

