# coding: utf-8

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from datetime import date

from associations.models import Association
from rooms.models import Room


class Event(models.Model):
    """
    Events are punctual activities organized by student associations.

    Such event's model contains a lot of potential practical information, from
    where the event takes place to the date and hour.
    """

    def check_future_event(event_date):
        """
        Checks if the newly created event is in the future.

        This function is used as event's date field validators. It prevents
        users from creating events in the past or the current day.

        :param event_date: date of the event to validate
        """

        if event_date <= date.today():
            raise ValidationError(
                "Impossible de créer un évènement dans le passé"
                "ou le jour même.")

    title = models.CharField("Titre de l'évènement", max_length=200)

    organizer = models.ForeignKey(Association)

    description = models.TextField("Description de l'évènement")

    PLACES_TYPES = (
        (0, "À l'EISTI de Cergy"),
        (1, "À l'EISTI de Pau"),
        # 2 to 9 are available for god knows what...
        (10, "À l'extérieur"),
    )
    location = models.SmallIntegerField(
        "Lieu de l'évènement", choices=PLACES_TYPES)

    address = models.TextField(
        "Adresse de l'évènement (si à l'extérieur)", blank=True, null=True)

    room = models.ForeignKey(
        Room, blank=True, null=True, verbose_name="Salle (si à l'EISTI)")

    day = models.DateField(
        "Jour de l'évènement", validators=[check_future_event])

    start_time = models.TimeField("Début (heure)")

    end_time = models.TimeField("Fin (heure)")

    authorized = models.BooleanField(
        "Évènement autorisé par l'administration", default=False)

    max_nb_participants = models.PositiveIntegerField(
        "Nombre de places disponibles", default=0)

    registered_only = models.BooleanField(
        "Sur inscription seulement", default=False)

    registration_cost = models.PositiveIntegerField(
        "Prix de l'inscription (laisser zéro si gratuit)", default=0)

    registration_closed = models.BooleanField(
        "Inscriptions closes (pas de places disponibles...)", default=False)

    cover_image = models.ImageField(
        "Image de couverture",
        upload_to='img/events/covers/',
        default='img/events/cover_default.png')

    affiche = models.ImageField(
        "Affiche ou bannière pour l'évènement",
        upload_to='evenements/associations/',
        blank=True, null=True)

    site = models.URLField(
        "Site promotionnel (renseignements supplémentaires, ou autres)",
        blank=True, null=True)

    participants = models.ManyToManyField(User, through='Registration')

    def is_past_event(self):
        """
        Returns True is the event was at least the day before.
        """
        return self.day < date.today()

    def registration_possible(self):
        """
        Returns True if it is possible to register to this event, meaning :
        1) the event in NOT in the past
        2) registrations are arbitrarily closed
        3) if the event requires registration, there are places left
        """
        remaining_places = self.max_nb_participants > self.participants.count()

        return not self.is_past_event() \
            and (not self.registration_closed) \
            and (not self.registered_only or remaining_places)

    def __unicode__(self):
        return self.title + " [" + self.organizer.__unicode__() + "]"



class Registration(models.Model):
    """
    Registration of a user to an event.

    A user should not be registered more than once to an event.
    """

    event = models.ForeignKey(Event)

    user = models.ForeignKey(User)

    registration_date = models.DateTimeField(
        "Date d'inscription", auto_now_add=True)

    paid = models.BooleanField(
        "Reglement effectué", default=False)

    def __unicode__(self):
        return str(self.user) + "@" + str(self.event)


class Discussion(models.Model):

    evenement = models.ForeignKey(Event, verbose_name="Sujet")

    auteur = models.ForeignKey(User, verbose_name="Auteur")

    texte = models.TextField("Message")

    date_publication = models.DateTimeField(
        "Date de publication", auto_now_add=True)

    def __unicode__(self):
        return self.date_publication.strftime("%a %d %b") + " : " \
            + str(self.auteur) + ">" + str(self.evenement)
