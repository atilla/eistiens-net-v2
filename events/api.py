#-*- coding: utf-8 -*-

from django.core.urlresolvers import reverse


def create_event_description(event):
    """
    Returns the text to show basic informations bout an event

    :param event: event object
    """

    event_url = reverse('events.views.display_event', args=(event.id,))

    return '''
        <div class="event-item well well-sm">
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <img src="%s" class="img-rounded">
                    <ul class="fa-ul">
                        <li><i class="fa-li fa fa-flag"></i>%s</li>
                    </ul>
                </div>
                
                <div class="event-infos-box col-md-6 col-sm-6">
                    <h3>%s</h3>
                    
                    <div class="row">
                        <ul class="fa-ul">
                            <li><i class="fa-li fa fa-calendar"></i>%s</li>
                            <li><i class="fa-li fa fa-clock-o"></i> %s - %s</li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-3 col-sm-3">
                    <a href="%s">
                        <button type="button" class="btn btn-primary">Infos et inscription</button>
                    </a>
                </div>
            </div>
        </div>''' % (event.organizer.logo.url, event.organizer,
            event.title, event.day.strftime('%d %B %Y'),
            event.start_time.strftime('%H:%M'),
            event.end_time.strftime('%H:%M'), event_url)