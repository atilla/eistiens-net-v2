# coding: utf-8
from django.contrib import admin
from events.models import *


admin.site.register(Event)
admin.site.register(Registration)
admin.site.register(Discussion)
