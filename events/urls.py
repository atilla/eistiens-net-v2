from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',

    url(r'^agenda$', 'events.views.agenda'),
    url(r'^calendrier$', 'events.views.calendar'),
    url(r'^calendrier_evenements', 'events.views.ajax_calendar'),
    url(r'^evenement/(?P<event_id>\w+)/$', 'events.views.display_event'),

    #URL for display panel (for exemple: EisTV)
    url(r'^agenda_panel$', 'events.views.agenda_panel'),

    url(r'^admin$', 'events.views.events_validations'),
    url(r'^admin/validate_all$', 'events.views.validate_all'),
    url(r'^admin/validate/(?P<event_id>\w+)/$', 'events.views.validate_one'),

    url(r'^evenement/(?P<event_id>\w+)/register$',
        'events.views.event_register'),
    url(r'^evenement/(?P<event_id>\w+)/unregister$',
        'events.views.event_unregister'),
)
