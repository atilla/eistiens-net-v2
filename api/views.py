# coding: utf-8

from django.shortcuts import render_to_response, get_object_or_404

from associations.models import Association  # Member


def association_board_members_name(request, association_name):
    association = get_object_or_404(Association, nom=association_name)
    board_members = association.get_board_members()

    return render_to_response(
        "members_list.json",
        {
            "members": board_members
        })


