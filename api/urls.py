from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',

    url(r'^asso/(?P<association_name>\w+)/board_members.json$',
        'api.views.association_board_members_name'),
)
