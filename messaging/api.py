from django.contrib.auth.models import User
from django.utils import timezone

from messaging.models import Thread, Message, PrivateThread, PrivateMessage

from eistiens_net.settings import MEDIA_ROOT

import os


def add_to_trie(word, leaf, trie):
    current = trie
    for letter in word.lower().replace(' ', ''):
        if letter not in current:
            current[letter] = {}

        current = current[letter]

    current['__leaf__'] = leaf


def generate_users_trie():
    trie = {}
    users = User.objects.all()

    for user in users:
        first, last = user.first_name, user.last_name
        leaf = {
            'id': user.username,
            'text': first + ' ' + last
        }

        add_to_trie(first + last, leaf, trie)
        add_to_trie(last + first, leaf, trie)

    return trie


def create_private_thread(subject, author, recipients, first_message):
    now = timezone.now()

    # Create the new thread and its first message
    thread = PrivateThread(
        subject=subject,
        last_message_date=now)
    thread.save()

    thread.participants.add(author)
    for recipient in recipients:
        if recipient != author:
            thread.participants.add(recipient)

    create_private_message(author, thread, first_message)

    return thread


def create_private_message(author, thread, content):
    now = timezone.now()

    thread.last_message_date = now
    thread.save()

    message = PrivateMessage(
        author=author,
        content=content,
        timestamp=now,
        parent_thread=thread
    )

    message.save()

    return message


def create_public_thread(subject, author, first_message):
    now = timezone.now()

    thread = Thread(
        subject=subject,
        last_message_date=now,
        creator=author)
    thread.save()

    create_public_message(author, thread, first_message)

    return thread


def create_public_message(author, thread, content):
    now = timezone.now()

    thread.last_message_date = now
    thread.save()

    message = Message(
        author=author,
        markdown_content=content,
        timestamp=now,
        parent_thread=thread)

    message.save()

    return message


def generate_image_filename(filename, path):
    """
    Find a place for the new image. If the image already exists,
    append an integer at the end of the filename.

    e.g: picture_2.png
    """
    filepath = os.path.join(MEDIA_ROOT, path, filename)

    if not os.path.isfile(filepath):
        return filepath, filename

    name, extension = os.path.splitext(filename)
    i = 1

    while True:
        filepath = os.path.join(MEDIA_ROOT, path, '%s_%d%s' % (name, i, extension))

        if not os.path.isfile(filepath):
            return filepath, '%s_%d%s' % (name, i, extension)

        i += 1


def upload(path, image):
    """
    Uploads an image on the server.
    """
    with open(path, 'wb+') as destination:
        for chunk in image.chunks():
            destination.write(chunk)
