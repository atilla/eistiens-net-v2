from twisted.internet.defer import inlineCallbacks
from autobahn.twisted.wamp import ApplicationSession

from django.utils import timezone
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from messaging.models import *
from messaging.api import create_private_message, create_public_message

import json


class AppSession(ApplicationSession):
    @inlineCallbacks
    def onJoin(self, details):
        # When a message is received, process it to put it in the database and publish to users
        def on_message_sent(message):
            try:
                # Load the published message { 'a': user.id, 'b': crossbar_topic, 'c': msg }
                content = json.loads(message)

                # Get the content from the JSON object
                user_id = int(content['a'])
                crossbar_topic = content['b']
                msg = content['c']

                if len(msg) == 0 or msg == '\n':
                    return

                # Get the user and the thread that correspond to the given content
                user = User.objects.get(pk=user_id)
                thread = PrivateThread.objects.get(crossbar_topic=crossbar_topic)

                # Add the message to the database
                create_private_message(user, thread, msg)

                # Spread the new message over all clients
                self.publish(crossbar_topic, json.dumps([{
                    'author': {
                        'url': reverse('profiles.views.display_profile',
                                            args=(user.username,)),
                        'name': user.first_name + ' ' + user.last_name
                    },
                    'time': timezone.localtime(timezone.now()).strftime('%H:%M'),
                    'content': msg
                }]))

            except Exception as e:
                print 'ERROR', e  ## TODO ##


        def on_last_messages_request(message):
            try:
                # Load the published message { 'a': connection_token, 'b': crossbar_topic, 'c': first_message, 'd': n_messages }
                content = json.loads(message)

                # Get the content from the JSON object
                connection_token = content['a']
                crossbar_topic = content['b']
                first_message = content['c']
                n_messages = content['d']
                scroll_down = content['e']

                # Query the thread that corresponds to the given content
                thread = PrivateThread.objects.get(crossbar_topic=crossbar_topic)

                # Query the messages of the topic
                messages_db = PrivateMessage.objects.filter(parent_thread=thread).order_by('-timestamp')
                messages = [{
                     'author': {
                        'url': reverse('profiles.views.display_profile',
                                        args=(msg.author.username,)),
                        'name': msg.author.first_name + ' ' + msg.author.last_name
                    },
                    'time': timezone.localtime(msg.timestamp).strftime('%H:%M'),
                    'content': msg.content,
                    'scroll_down': scroll_down
                } for msg in messages_db[first_message:first_message + n_messages]]

                # Spread the messages to all clients
                self.publish(connection_token, json.dumps(messages))

            except Exception as e:
                print 'ERROR', e  ## TODO ##


        def on_public_message(message):
            try:
                content = json.loads(message)

                csrf_token, user_id = content['a'], int(content['b'])
                content, thread_id = content['c'], int(content['d'])

                user = User.objects.get(pk=user_id)
                thread = Thread.objects.get(id=thread_id)

                message = create_public_message(user, thread, content)

                self.publish('pb_thread_%d' % thread_id, json.dumps({
                    'user': {
                        'name': user.first_name + ' ' + user.last_name,
                        'picture': user.profile.avatar.url
                    },
                    'timestamp': timezone.localtime(timezone.now()).strftime('%d %b %Y %H:%M'),
                    'content': message.content
                }))
            except:
                print 'ERROR', e ## TODO ##


        # Subscribe to 'eistiens.net.message.sent', whenever a message is sent, we call the 'on_message_sent' method
        yield self.subscribe(on_message_sent, 'eistiens.net.message.sent')

        # Subscribe to 'eistiens.net.last_messages', whenever a user wants to query the last messages of a thread
        yield self.subscribe(on_last_messages_request, 'eistiens.net.last_messages')

        # Subscribe to 'eistiens.net.message.public', whenever a public message is sent, we call the 'on_public_message' method
        yield self.subscribe(on_public_message, 'eistiens.net.message.public')
