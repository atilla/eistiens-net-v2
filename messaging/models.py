# coding: utf-8

from django.contrib.auth.models import User
from django.db import models

import random, string, markdown2

from jobs.api import sanitize


class Thread(models.Model):

    """
    Threads are groups of messages.
    """

    subject = models.CharField("Thread object", max_length=100)

    timestamp = models.DateTimeField(
        "Creation date", auto_now_add=True, editable=False)

    last_message_date = models.DateTimeField(
        "Date du dernier message", auto_now_add=True, editable=True)

    views = models.PositiveIntegerField('Nombre de vues', default=0)

    creator = models.ForeignKey(User)

    def get_messages(self):
        """
        Returns the thread's messages.
        """
        return Message.objects.filter(message_type=0, parent_thread=self.id)

    def __unicode__(self):
        return u'%s (%s)' % (unicode(self.subject), unicode(self.timestamp))


class PrivateThread(models.Model):

    """
    Private conversation.
    """

    subject = models.CharField("PM object", max_length=100)

    crossbar_topic = models.CharField("Crossbar", max_length=255,
        blank=True, null=True, default='')

    timestamp = models.DateTimeField(
        "Creation date", auto_now_add=True, editable=False)

    last_message_date = models.DateTimeField(
        "Date du dernier message", auto_now_add=True, editable=True)

    participants = models.ManyToManyField(User)

    def get_messages(self):
        """
        Returns the conversation's messages.
        """
        return PrivateMessage.objects.filter(parent_thread=self.id)

    def save(self, *args, **kwargs):
        super(PrivateThread, self).save(*args, **kwargs)

        # If it is the first time, we generate the `crossbar_topic`
        if len(self.crossbar_topic) == 0:
            random_seed = ''.join(random.choice(string.ascii_uppercase) for _ in xrange(42))
            self.crossbar_topic = 'pvtopic_%d_%s' % (self.id, random_seed)
            super(PrivateThread, self).save(*args, **kwargs)


    def __unicode__(self):
        return u"[PM] %s (%s)" % (unicode(self.subject), unicode(self.timestamp))


class Message(models.Model):

    """
    Messages are the atoms present in a thread.
    """

    author = models.ForeignKey(User)

    content = models.TextField("Contenu du message", blank=True)

    markdown_content = models.TextField("Contenu du message (markdown)")

    timestamp = models.DateTimeField(
        "Redaction date", auto_now_add=True, editable=False)

    parent_thread = models.ForeignKey(Thread)

    def save(self, *args, **kwargs):
        self.content = markdown2.markdown(self.markdown_content)
        self.content = sanitize(self.content)
        super(Message, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"%s (%s)" % (unicode(self.author), unicode(self.timestamp))


class PrivateMessage(models.Model):

    """
    Private messages are the atoms present in a private conversation.
    """

    author = models.ForeignKey(User)

    content = models.TextField("Contenu du message", max_length=2000)

    timestamp = models.DateTimeField(
        "Redaction date", auto_now_add=True, editable=False)

    parent_thread = models.ForeignKey(PrivateThread)

    def __unicode__(self):
        return u"%s (%s)" % (unicode(self.author), unicode(self.timestamp))