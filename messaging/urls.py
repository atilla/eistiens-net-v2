from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',

    url(r'^publics$', 'messaging.views.publicthreads'),
    url(r'^publics/(?P<thread_id>\w+)$', 'messaging.views.thread'),

    url(r'^prives$', 'messaging.views.privatethreads'),
    url(r'^prives_utilisateurs$', 'messaging.views.private_thread_users'),

    url(r'^trie$', 'messaging.views.users_trie'),
    url(r'^creer_conversation$', 'messaging.views.create_new_private_thread'),
    url(r'^supprimer_conversation', 'messaging.views.delete_private_thread'),
    url(r'^supprimer_toutes_conversations',
        'messaging.views.delete_all_private_threads'),

    url(r'^upload-image$', 'messaging.views.upload_image'),
)
