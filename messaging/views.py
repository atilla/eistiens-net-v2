# coding: utf-8

from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.utils import timezone
from django.views.decorators.cache import cache_page
from django.contrib.auth.models import User
from django.middleware import csrf

from messaging.api import create_private_thread, create_public_thread
from messaging.api import generate_image_filename, upload, generate_users_trie

from messaging.models import Thread, PrivateThread, PrivateMessage, Message
from profiles.models import Profile

import json
import os


@login_required
def privatethreads(request):
    """
    Displays all the private threads of the connected user.

    Can be used with a GET method to prepare the modal to start a new
    conversation: if the user clicks on 'Envoyer un message' on the profile
    of another user.

    :param request: request object
    """
    if request.method == 'GET' and 'destinataire' in request.GET:
        recipient = request.GET['destinataire']
    else:
        recipient = None

    try:
        profile = Profile.objects.get(user=request.user)
        profile.last_private_messages_check = timezone.now()
        profile.save()
    except:
        pass  # For instance, we are admin -> Maybe no profile

    threads = PrivateThread.objects.filter(
        participants=request.user).order_by('-last_message_date')

    return render_to_response(
        'private_thread.html',
        {
            'request': request,
            'threads': threads,
            'message_sent': 'eistiens.net.message.sent',
            'request_last_messages': 'eistiens.net.last_messages',
            'recipient': recipient
        }, context_instance=RequestContext(request))


@login_required
def publicthreads(request):
    """
    Displays the public threads to the user, sorted by last message date desc

    Handles POST request to create a new thread when the user clicks the
    "Nouveau thread" button.

    :param request: request object
    """
    error = False

    if request.method == 'POST':
        POST = request.POST

        result = {
            'redirect': reverse('messaging.views.publicthreads')
        }

        if all(x in POST for x in ('csrfmiddlewaretoken', 'subject', 'message')):
            csrf_token = POST['csrfmiddlewaretoken']
            subject, message = POST['subject'], POST['message']

            if csrf_token == csrf.get_token(request):
                thread = create_public_thread(subject, request.user, message)
                result['redirect'] = reverse('messaging.views.thread', args=(thread.id,))
        

        return HttpResponse(json.dumps(result), content_type="application/json")

    threads = Thread.objects.all().order_by('-last_message_date')

    return render_to_response(
        'threads.html',
        {
            'request': request,
            'threads': threads,
            'error': error
        }, context_instance=RequestContext(request))


@login_required
def thread(request, thread_id):
    """
    Displays a public thread

    :param request: request object
    :param thread_id: id of the thread to show
    """

    thread = get_object_or_404(Thread, id=thread_id)

    thread.views += 1
    thread.save()

    return render_to_response(
        'public_thread.html',
        {
            'request': request,
            'thread': thread,
            'send_message': 'eistiens.net.message.public'
        }, context_instance=RequestContext(request))


@login_required
def upload_image(request):
    result = {
        'success': True,
        'url': ''
    }

    if request.method == 'POST':
        FILES = request.FILES

        # Only allow images with size <= 5 MiB
        if 'image' not in FILES or FILES['image'].size > 5 * 1024 * 1024:
            result['success'] = False
            result['message'] = 'Aucune image ou image trop lourde (Max 5 Mo)'

        else:
            image = FILES['image']
            _, extension = os.path.splitext(image.name)

            if extension not in ('.jpg', '.jpeg', '.gif', '.png', '.bmp'):
                result['success'] = False
                result['message'] = 'Format %s non supporté' % extension

            else:
                path = 'img/messages'
                image_path, filename = generate_image_filename(image.name, path)
                upload(image_path, image)
                result['url'] = os.path.join('/media', path, filename)

    else:
        result['success'] = False
        result['message'] = 'Pas une requete POST'
    
    return HttpResponse(json.dumps(result), content_type="application/json")


@login_required
def private_thread_users(request):
    """
    Returns a JSON string containing the users in a given thread.
    """

    error = HttpResponse(json.dumps({ 'success': False }),
        content_type="application/json")

    if request.method != 'GET' or 'a' not in request.GET:
        return error

    # Get the content from the request
    crossbar_topic = request.GET['a']

    # Query the thread that corresponds to the given content
    if not PrivateThread.objects.filter(crossbar_topic=crossbar_topic).exists():
        return error

    thread = PrivateThread.objects.get(crossbar_topic=crossbar_topic)

    # The user has nothing to do here if it is not a participant of the thread
    if request.user not in thread.participants.all():
        return error

    # List the users
    users = [{
        'name': u.first_name + ' ' + u.last_name,
        'url': reverse('profiles.views.display_profile', args=(u.username,))
    } for u in thread.participants.all().order_by('first_name', 'last_name')]

    # JSON content that will be returned
    result = {
        'success': True,
        'users': users
    }

    return HttpResponse(json.dumps(result), content_type="application/json")


@login_required
@cache_page(60) ## TODO: Increase when more users will be in the DB ##
def users_trie(request):
    """
    Returns a trie containing all users in the database
    (first name + last name and last name + first name).
    """
    return HttpResponse(json.dumps(generate_users_trie()),
        content_type="application/json")


@login_required
def create_new_private_thread(request):
    """
    Creates a new private thread
    """
    if request.method == 'GET':
        subject = request.GET.get('subject', '')
        message = request.GET.get('message', '')
        recipients_strings = request.GET.get('recipients', '').split(',')
        csrf_token = request.GET.get('csrf', '')

        if csrf_token == csrf.get_token(request):
            recipients = []

            for recipient in recipients_strings:
                try:
                    recipients.append(User.objects.get(username=recipient))
                except: # User does not exist -> We have a jerk here
                    pass

            # Prevent bad request, happens if the user is a jerk
            if all(len(x) > 0 for x in (subject, message, recipients)):
                create_private_thread(
                    subject, request.user, recipients, message)

    return HttpResponse(json.dumps({
            'redirect': reverse('messaging.views.privatethreads'),

        }), content_type='application/json')


@login_required
def delete_private_thread(request):
    """
    Deletes a private thread from a user.
    """
    if request.method != 'GET':
        return HttpResponseRedirect(reverse('messaging.views.privatethreads'))

    csrf_token = request.GET.get('csrf', '')
    crossbar_topic = request.GET.get('a', '')

    if csrf_token != csrf.get_token(request):
        return HttpResponseRedirect(reverse('messaging.views.privatethreads'))

    if PrivateThread.objects.filter(crossbar_topic=crossbar_topic).exists():
        thread = PrivateThread.objects.get(crossbar_topic=crossbar_topic)

        if request.user in thread.participants.all():
            thread.participants.remove(request.user)

    return HttpResponseRedirect(reverse('messaging.views.privatethreads'))


@login_required
def delete_all_private_threads(request):
    """
    Deletes all private threads from a user.
    """
    if request.method != 'GET':
        return HttpResponseRedirect(reverse('messaging.views.privatethreads'))

    csrf_token = request.GET.get('csrf', '')

    if csrf_token != csrf.get_token(request):
        return HttpResponseRedirect(reverse('messaging.views.privatethreads'))

    for thread in PrivateThread.objects.filter(participants=request.user):
        thread.participants.remove(request.user)

    return HttpResponseRedirect(reverse('messaging.views.privatethreads'))
