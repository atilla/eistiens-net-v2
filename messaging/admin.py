from django.contrib import admin
from models import *


admin.site.register(Thread)
admin.site.register(PrivateThread)
admin.site.register(Message)
admin.site.register(PrivateMessage)
