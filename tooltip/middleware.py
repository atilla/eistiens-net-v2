#coding: utf-8

class Tooltip(object):
    def __init__(self):
        self.is_active, self.has_icon, self.has_button = False, False, False
        self.icon, self.title, self.text = '', '', ''
        self.button_text, self.button_url = '', ''

    def set_content(self, title, text):
        self.is_active = True
        self.title = title
        self.text = text

    def set_icon(self, icon):
        self.has_icon = True
        self.icon = icon

    def set_button(self, text, url):
        self.has_button = True
        self.button_text, self.button_url = text, url


class TooltipMiddleware(object):
    """
    Middleware that handles tooltips.
    """

    def process_request(self, request):
        request._tooltip = Tooltip()
