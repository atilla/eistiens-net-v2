from django.core.urlresolvers import reverse


def content(request, title, text):
    """
    Text of the tooltip
    """
    if hasattr(request, '_tooltip'):
        request._tooltip.set_content(title, text)


def icon(request, icon):
    """
    Icon on the left of the tooltip
    """
    if hasattr(request, '_tooltip'):
        request._tooltip.set_icon(icon)


def button(request, text, view):
    """
    Button to redirect to a given view
    """
    if hasattr(request, '_tooltip'):
        request._tooltip.set_button(text, reverse(view))


def get_tooltip(request):
    if hasattr(request, '_tooltip'):
        return request._tooltip
    return None