#coding: utf-8

from tooltip.api import get_tooltip


def tooltip(request):
    return {
        'tooltip': get_tooltip(request)
    }
