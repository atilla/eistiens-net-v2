#!/bin/bash

case $1 in
crossbar)
	export DJANGO_SETTINGS_MODULE="eistiens_net.settings"
	`crossbar start`
	;;
basic)
	`./manage.py runserver $2`
	;;
prod)
	export DJANGO_SETTINGS_MODULE="eistiens_net.settings_prod"
	`crossbar start`
esac
