# coding: utf-8
from django.contrib import admin
from jobs.models import Enterprise, Contact, Question, Review

admin.site.register(Enterprise)
admin.site.register(Contact)
admin.site.register(Question)
admin.site.register(Review)
