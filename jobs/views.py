#coding: utf-8

from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
import tooltip

from jobs.models import Contact, Enterprise, Question, Review

import json


@login_required
def jobs(request):
    """
    Display a page where a user can see enterprises and their
    reviews/contacts/questions and create a new entry

    :param request: request object
    """

    enterprises = Enterprise.objects.all().order_by('name')

    tooltip.content(request,
        title="""Découvrez la base de données des entreprises !""",
        text="""Vous avez un stage à faire, ou voulez simplement vous informer
            sur les pratiques de certaines entreprises ? Sur cette page vous
            pouvez accéder à diverses informations sur les entreprises dans
            lesquelles des eistiens ont déjà candidaté: leurs critiques ainsi
            que les questions qui leur ont été posées lors de leur
            entretien.""")
    tooltip.icon(request, 'building-o')

    return render_to_response(
        'entreprises.html',
        {
            'request': request,
            'enterprises': enterprises
        }, context_instance=RequestContext(request))


@login_required
def add_entry(request):
    """
    Displays forms to let the user add an entry to the Jobs DB

    :param request: request objects
    """

    error = False

    if request.method == 'POST':
        other = ['other_name', 'other_location', 'other_description',
            'other_website']

        other_data = [request.POST.get(x, '') for x in other]

        if any(len(x) for x in other_data):
            if any(not len(x) for x in other_data):
                messages.error(request, u'Vous devez remplir tous les champs')
                error = True
            else:
                kwargs = {
                    'name': other_data[0],
                    'website': other_data[3],
                    'location': other_data[1],
                    'markdown_description': other_data[2]
                }

                if 'other_logo' in request.FILES:
                    kwargs['logo'] = request.FILES['other_logo']

                enterprise = Enterprise(**kwargs)
                enterprise.save()
                enterprise.users.add(request.user)
        else:
            try:
                enterprise = Enterprise.objects.get(
                    id=int(request.POST['enterprise']))
            except:
                messages.error(request, u'L\'entreprise n\'existe pas')
                error = True

        if not error:
            return HttpResponseRedirect(reverse_lazy(
                'jobs.views.update_enterprise',
                args=(enterprise.id,)))


    enterprises = Enterprise.objects.all().order_by('name')

    return render_to_response(
        'ajouter.html', {
            'request': request,
            'enterprises': enterprises
        }, context_instance=RequestContext(request))


@login_required
def update_enterprise(request, enterprise_id):
    """
    Allows a user to add reviews and interview questions for an enterprise.

    :param request: request object
    :param enterprise_id: id of the enterprise
    """

    enterprise = get_object_or_404(Enterprise, id=enterprise_id)

    if request.method == 'POST':
        success = True
        post_type = request.POST.get('type', '')

        if post_type == 'review':
            required = ['review', 'wage', 'year']
            required_data = [request.POST.get(x, '') for x in required]

            if any(not len(x) for x in required_data) or not required_data[2].isdigit():
                success = False
            else:
                review = Review(
                    enterprise=enterprise,
                    user=request.user,
                    markdown_review=required_data[0],
                    wage=required_data[1],
                    year=int(required_data[2]))
                review.save()

        elif post_type == 'question':
            required = ['question', 'role']
            required_data = [request.POST.get(x, '') for x in required]

            if any(not len(x) for x in required_data):
                success = False
            else:
                question = Question(
                    enterprise=enterprise,
                    user=request.user,
                    markdown_text=required_data[0],
                    requested_role=required_data[1])
                question.save()

        else:
            success = False

        if success and request.user not in enterprise.users.all():
            enterprise.users.add(request.user)

        return HttpResponse(json.dumps(
            {
                'success': success
            }), content_type='application/json')

    return render_to_response(
        'editer.html', {
            'request': request,
            'enterprise': enterprise,
            'years': Review.YEARS
        }, context_instance=RequestContext(request))


@login_required
def enterprise(request, enterprise_id):
    """
    Displays informations on an enterprise, contacts, interview questions
    and reviews.

    :param request: request object
    :param enterprise_id: id of the enterprise
    """

    enterprise = get_object_or_404(Enterprise, id=enterprise_id)
    
    reviews = Review.objects.filter(
        enterprise=enterprise_id).order_by('-publication_date')

    questions = Question.objects.filter(
        enterprise=enterprise_id).order_by('-publication_date')

    contacts = Contact.objects.filter(enterprise=enterprise_id)

    if request.method == 'POST':
        data_type, data_id = None, None

        if 'id' not in request.POST:
            messages.error(request, u'Formulaire invalide')
        else:
            form_id = request.POST['id'].split()
            if len(form_id) != 2 or not form_id[1].isdigit():
                messages.error(request, u'Formulaire invalide')
            else:
                data_type, data_id = form_id[0], int(form_id[1], 10)

        # The user wants to edit a review
        if data_type == 'review' and Review.objects.filter(id=data_id).exists():
            required = ['review', 'wage', 'year']
            required_data = [request.POST.get(x, '') for x in required]

            if any(not len(x) for x in required_data):
                messages.error(request, u'Un des champs est vide')
            else:
                review = Review.objects.get(id=data_id)

                if review.user != request.user:
                    messages.error(request, u'Formulaire invalide')
                else:
                    review.markdown_review = required_data[0]
                    review.wage = required_data[1]
                    review.year = int(required_data[2], 10)
                    review.save()

        # The user wants to edit a question
        elif data_type == 'question' and Question.objects.filter(id=data_id).exists():
            required = ['question', 'role']
            required_data = [request.POST.get(x, '') for x in required]

            if any(not len(x) for x in required_data):
                messages.error(request, u'Un des champs est vide')
            else:
                question = Question.objects.get(id=data_id)

                if question.user != request.user:
                    messages.error(request, u'Formulaire invalide')
                else:
                    question.markdown_text = required_data[0]
                    question.requested_role = required_data[1]
                    question.save()

        else:
            messages.error(request, u'Formulaire invalide')

    return render_to_response(
        'entreprise.html',
        {
            'request': request,
            'enterprise': enterprise,
            'reviews': reviews,
            'questions': questions,
            'contacts': contacts
        }, context_instance=RequestContext(request))


@login_required
def delete_review(request, review_id):
    """
    Deletes a given review

    :param request: request object
    :param review_id: id of the review
    """

    review = get_object_or_404(Review, id=review_id)
    enterprise_id = review.enterprise.id

    if request.user != review.user:
        messages.error(request, u'Impossible de supprimer la critique')
    else:
        review.delete()

    return HttpResponseRedirect(reverse_lazy(
        'jobs.views.enterprise', args=(enterprise_id,)))


@login_required
def delete_question(request, question_id):
    """
    Deletes a given question

    :param request: request object
    :param question_id: id of the question
    """

    question = get_object_or_404(Question, id=question_id)
    enterprise_id = question.enterprise.id

    if request.user != question.user:
        messages.error(request, u'Impossible de supprimer la question')
    else:
        question.delete()

    return HttpResponseRedirect(reverse_lazy(
        'jobs.views.enterprise', args=(enterprise_id,)))
