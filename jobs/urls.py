from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',

    url(r'^$', 'jobs.views.jobs'),
    url(r'^ajouter$', 'jobs.views.add_entry'),
    url(r'^entreprise/editer/(?P<enterprise_id>\w+)$', 'jobs.views.update_enterprise'),
    url(r'^entreprise/(?P<enterprise_id>\w+)$', 'jobs.views.enterprise'),
    url(r'^supprimer_critique/(?P<review_id>\w+)$', 'jobs.views.delete_review'),
    url(r'^supprimer_question/(?P<question_id>\w+)$', 'jobs.views.delete_question'),
)
