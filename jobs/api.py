import re


# XSS_RE = re.compile('<(script|img).*>(.*</script>|)', re.IGNORECASE)

XSS_RE = re.compile('<script.*>(.*</script>)', re.IGNORECASE)


def sanitize(s):
    """
    Prevents XSS, removing all script and img tags

    TODO: <a href
    """

    xss = XSS_RE.search(s)
    while xss is not None:
        s = s[:xss.start()] + s[xss.end():]
        xss = XSS_RE.search(s)

    return s
