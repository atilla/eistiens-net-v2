# coding: utf-8

import markdown2

from django.contrib.auth.models import User
from django.db import models

from jobs.api import sanitize


class Enterprise(models.Model):
    """
    An enterprise is made up of a name, a logo and a location,
    users can add reviews, interview questions and contacts they have
    within a given enterprise.
    """

    name = models.CharField('Nom de l\'entreprise', max_length=256)

    logo = models.ImageField(
        'Logo de l\'entreprise',
        upload_to='img/enterprises/logos/',
        default='img/enterprises/default_logo_128.png')

    website = models.URLField('Site internet', blank=True, null=True)

    location = models.CharField(
        'Lieu de l\'entreprise (Ville, Pays)', max_length=256)

    markdown_description = models.TextField(
        'Brève description de ce que fait l\'entreprise (markdown)')

    description = models.TextField(
        'Brève description de ce que fait l\'entreprise', blank=True)

    users = models.ManyToManyField(User)

    def save(self, *args, **kwargs):
        self.description = markdown2.markdown(self.markdown_description)
        self.description = sanitize(self.description)
        super(Enterprise, self).save(*args, **kwargs)

    def __unicode__(self):
        return unicode(self.name) + ' - ' + self.location


class Contact(models.Model):
    """
    A contact in an enterprise, represented as a name, a role in the
    enterprise and telephone number + mail.
    """

    enterprise = models.ForeignKey(Enterprise, verbose_name='Entreprise')

    user = models.ForeignKey(User, verbose_name='Eistien')

    name = models.CharField('Nom du contact', max_length=256)

    role = models.CharField('Poste dans l\'entreprise', max_length=256)

    mail = models.CharField('Adresse e-mail', max_length=256)

    phone = models.CharField('Numéro de téléphone', max_length=256)

    def __unicode__(self):
        return unicode(self.name) + ' - ' + unicode(self.enterprise)


class Question(models.Model):
    """
    A question asked during an interview, represented as a text and the role the user
    was applying for.
    """

    enterprise = models.ForeignKey(Enterprise, verbose_name='Entreprise')

    user = models.ForeignKey(User, verbose_name='Eistien')

    markdown_text = models.TextField('Question (markdown)')

    text = models.TextField('Question', blank=True)

    requested_role = models.CharField(
        'Candidature pour un poste de', max_length=256)

    publication_date = models.DateTimeField(
        'Date de publication', auto_now_add=True)

    def save(self, *args, **kwargs):
        self.text = markdown2.markdown(self.markdown_text)
        self.text = sanitize(self.text)
        super(Question, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'Question posée à ' + unicode(self.user)


class Review(models.Model):
    """
    The review of an enterprise, a review text, the wage of the user,
    the year of the user when he/she did the internship (ING1, ING2, ING3)
    """

    enterprise = models.ForeignKey(Enterprise, verbose_name='Entreprise')

    user = models.ForeignKey(User, verbose_name='Eistien')

    markdown_review = models.TextField('Critique (positive ou négative) du stage (markdown)')

    review = models.TextField('Critique (positive ou négative) du stage', blank=True)

    wage = models.CharField('Salaire', max_length=256)

    YEARS = (
        (0, 'CPI'),
        (1, 'ING1'),
        (2, 'ING2'),
        (3, 'ING3'),
        (4, 'Autre'),
    )
    year = models.SmallIntegerField('Année d\'étude', choices=YEARS)

    publication_date = models.DateTimeField(
        'Date de publication', auto_now_add=True)

    def save(self, *args, **kwargs):
        self.review = markdown2.markdown(self.markdown_review)
        self.review = sanitize(self.review)
        super(Review, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'Critique par ' + unicode(self.user)
