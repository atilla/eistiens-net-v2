Eistiens.net v2
===============

This version is Pony Powered ! Hack it at your own risks !

Getting started
===============

``` sh
  $ git clone git@gitlab.etude.eisti.fr:atilla/eistiens-net.git
  $ sudo apt-get install python-virtualenv
  $ cd eistiens_net
  $ git checkout dev
  $ virtualenv py-enet
  $ source py-enet/bin/activate
  $ # Requirements for PIL image formats support
  $ sudo apt-get -y install python-dev libsasl2-dev libjpeg-dev libwebp-dev libpng-dev libldap-dev
  $ pip install -r requirements.txt
  $ # Install Dev tools requirements
  $ sudo apt-get -y install  graphviz-dev pkg-config
  $ pip install -r requirements-dev.txt
  $ # Tweak config if needed
  $ ./manage.py syncdb
  $ ./manage.py migrate
  $ ./import_data.sh
  $ ./run.sh crossbar
  $ firefox http://localhost:14256/ &
```

For guests
==========

As a guest (what you normally are if you're any EISTI student),
you can create issues, comment them, and fork the repository.

For guests who want to contribute to this great project, don't
hesitate to fork the repository and submit some merge requests.  After
a certain amount of meaningful commits (bugfixes, features, etc...)
you are encouraged to ask the rank of developer. Then you will be able
to commit directly in the `dev` branch. 
