# coding: utf-8
from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',

    url(r'^liste$', 'perdus_trouves.views.liste'),
    url(r'^fiche$', 'perdus_trouves.views.fiche'),
)
