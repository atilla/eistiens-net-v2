# coding: utf-8

from django.shortcuts import render_to_response, get_object_or_404

from perdus_trouves.models import Ticket


def liste(request):
    """
    Displays tickets for both found and lost items.
    """

    tickets = Ticket.objects.all().order_by('id')
    return render_to_response(
        "blackshop.html",
        {
            'request': request,
            'liste': tickets
        })


def fiche(request, perdu_id):
    """
    Displays all details on an items (either lost or found).

    :param request: request object
    :param perdu_id: ticket id
    """

    objet = get_object_or_404(Ticket, id=perdu_id)
    return render_to_response(
        "objet.html",
        {
            'request': request,
            'objet': objet
        })
