# coding: utf-8

from django.db import models
from django.contrib.auth.models import User


class Ticket(models.Model):
    """
    A ticket is a request relative to something lost or found.
    """

    TICKET_TYPE = (
        (0, "Objet perdu"),
        (1, "Objet trouvé")
    )
    type_ticket = models.SmallIntegerField(
        "Type du ticket", choices=TICKET_TYPE)

    auteur = models.ForeignKey(User, verbose_name="Auteur du ticket")

    nom = models.CharField(
        "Intitulé du ticket (nom de l'objet perdu/trouvé", max_length=200)

    description = models.TextField("Description de l'objet trouvé/perdu")

    image = models.ImageField(
        "Image optionnelle de l'objet",
        upload_to='objets_perdus', blank=True, null=True)

    date_ouverture = models.DateTimeField(auto_now_add=True)

    date_fermeture = models.DateTimeField(
        "Date de fermeture du ticket (défini quand marqué résolu",
        blank=True, null=True)

    resolu = models.BooleanField("Vrai si le ticket est clos", default=False)

    def __unicode__(self):
        return self.get_type_ticket_display() + " : " + self.nom


class Reponse(models.Model):

    auteur = models.ForeignKey(User, verbose_name="Auteur")

    ticket = models.ForeignKey(Ticket, verbose_name="Ticket concerné")

    date = models.DateTimeField("Date de rédaction", auto_now_add=True)

    message = models.TextField("Réponse")
