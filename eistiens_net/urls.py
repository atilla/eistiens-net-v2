from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings


admin.autodiscover()

urlpatterns = patterns(
    '',

    # Associations app
    url(r'^associations/', include('associations.urls')),

    # Events app
    url(r'^evenements/', include('events.urls')),

    # Profiles app
    url(r'^profils/', include('profiles.urls')),

    # Messages
    url(r'^messages/', include('messaging.urls')),

    # Schedules
    url(r'^edt/', include('schedule.urls')),

    # API
    url(r'^api/', include('api.urls')),

    # local
    url(r'^locaux/', include('rooms.urls')),

    # Jobs
    url(r'^jobs/', include('jobs.urls')),

    # Facts
    url(r'^facts/', include('facts.urls')),

    # Portal app
    url(r'^', include('portal.urls')),

    # Medias
    # ! FOR TEST SERVER ONLY !
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT}),

    # Grapelli urls : admin panel template
    url(r'^grappelli/', include('grappelli.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
