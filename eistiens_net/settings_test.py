#!/usr/bin/env python

from eistiens_net.settings import *


INSTALLED_APPS += (
    'django_jenkins',
)

BLACKLISTED_APPS = (
    'debug_toolbar',
)

INSTALLED_APPS = tuple(app for app in INSTALLED_APPS
                       if app not in BLACKLISTED_APPS)

PROJECT_APPS = (
    'api',
    'associations',
    'eistiens_net',
    'events',
    'rooms',
    'perdus_trouves',
    'portal',
    'profiles',
)

DATABASES['test'] = {
    # Possible engines : 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
    'ENGINE': 'django.db.backends.sqlite3',
    'NAME': 'test_db.sqlite',
    'USER': '',
    'PASSWORD': '',
    'HOST': '',
    'PORT': '',
}

JENKINS_TASKS = (
    'django_jenkins.tasks.with_coverage',
    'django_jenkins.tasks.django_tests',
    'django_jenkins.tasks.run_pylint',
    'django_jenkins.tasks.run_pep8',
    'django_jenkins.tasks.run_pyflakes',
)
