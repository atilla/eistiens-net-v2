"""
Utilitary functions that may be useful site-wide.

If you have a need for some general purpose function, this file should be the
right place to add it.
"""

from datetime import datetime
from eistiens_net.settings import MEDIA_URL


def get_current_year():
    """
    Returns the current year.

    The year sept. 2042 - sept. 2043 will be labelled as "2042".
    This function is really necessary for association membership (which
    depends on the year). See association module for more information.
    """

    now = datetime.today()

    if now < datetime(now.year, 9, 1):
        return now.year-1

    else:
        return now.year


def get_media_folder():
    """
    Returns the path to the media folder.

    This is used for instance to get absolute url to some medias.
    """

    return MEDIA_URL
