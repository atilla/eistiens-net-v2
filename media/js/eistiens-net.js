
/***** PRIVATE MESSAGEING *****/


/* Connects to the WAMP server and return the connection object */
function newConnection() {
    /* The URL of the WAMP Router (Crossbar.io) */
    var wsuri;
    if (document.location.origin == "file://") {
        wsuri = "ws://127.0.0.1:14256/ws";
    }
    else {
        wsuri = (document.location.protocol === "http:" ? "ws:" : "wss:") + "//" +
        document.location.host + "/ws/";
    }

    /* The WAMP connection to the Router */
    return new autobahn.Connection({
        url: wsuri,
        realm: "realm1"
    });
}

/* Treats a message (add urls, newlines, ...) and add the content to the div
    passed in parameter */
function treatMessage(div, content) {
    /* Regex to find urls and newlines -> let them be seen correctly by the user */
    var urlRegex = /(https?:\/\/[^\s]+)|\n/g;

    /* The position in the string of the first character of the current child */
    var start = 0;

    /* All occurences of urls and newlines */
    while ((match = urlRegex.exec(content)) !== null) {
        div.appendChild(document.createTextNode(content.substring(start, match.index)));

        /* It is a newline */
        if (match.index == urlRegex.lastIndex - 1) {
            div.appendChild(document.createElement('br'));
        }
        /* It's a link */
        else {
            /* Real url */
            var url = content.substring(match.index, urlRegex.lastIndex);

            var link = document.createElement('a');
            link.setAttribute('href', url);
            link.appendChild(document.createTextNode(url));
            div.appendChild(link);
        }

        start = urlRegex.lastIndex;
    }
    
    div.appendChild(document.createTextNode(content.substring(start)));
}


/* Creates a div containing a message (private thread) */
function createMessage(message) {
    /* Create a new message entry */
    var div = document.createElement('div');
        div.setAttribute('class', "row private-message");

    /* Create the link to the author's profile */
    var a = document.createElement('a');
        a.setAttribute('href', message.author.url);
        a.innerHTML = message.author.name;

    /* Add the timestamp */
    var timestamp = document.createElement('span');
        timestamp.setAttribute('class', 'timestamp');
        timestamp.innerHTML = message.time;

    div.appendChild(a);
    div.appendChild(document.createTextNode(' — '));
    div.appendChild(timestamp);
    div.appendChild(document.createElement('br'));

    /* Treat the message (add links, newlines, ...) */
    treatMessage(div, message.content);

    return div;
}

