# coding: utf-8

from django.db import models

import associations.models


class Buildind(models.Model):
    """
    A building represents a real-life building.

    Each building must be attached to a campus. It has several basic
    informations such as the number of floors. Typically, buildings are
    labelled with a two letter name. For example Condorcet building becomes
    "CT"
    """

    name = models.CharField("Nom du bâtiment", max_length=100)

    abbreviation = models.CharField("Abréviation", max_length=5)

    CAMPUS = (
        (0, "Cergy"),
        (1, "Pau")
        )
    campus = models.SmallIntegerField("Campus", choices=CAMPUS)

    nb_floors = models.PositiveSmallIntegerField("Nombre d'étages")

    def __unicode__(self):
        return self.name + " (" + self.abbreviation + ") - " + \
            self.get_campus_display()


class Room(models.Model):
    """
    A room represents a room in one of the campus' buildings.
    """

    name = models.CharField("Nom/numéro de la salle", max_length=40)

    building = models.ForeignKey(Buildind, verbose_name="Bâtiment")

    floor = models.PositiveSmallIntegerField("Étage")

    nb_seats = models.PositiveIntegerField(
        "Nombre de places", blank=True, null=True)

    has_projector = models.BooleanField(
        "Possède un vidéoprojecteur", default=False)

    is_amphitheatre = models.BooleanField("Est un amphi", default=False)

    has_control_room = models.BooleanField("Possède une régie", default=False)

    has_electric_plugd = models.BooleanField(
        "Prises électriques", default=True)

    has_network_plugs = models.BooleanField("Prises réseau", default=True)

    unavailable = models.BooleanField(
        "Salle indisponible (local, travaux, dégradation, etc.)",
        default=False)

    comments = models.TextField("Remarques", blank=True)

    def get_associations(self):
        """
        Returns a list (query set) of associations using this room as dedicated
        room.
        """

        return associations.models.Association.objects.filter(room=self)

    def __unicode__(self):
        return self.building.abbreviation + self.name


class RoomGroup(models.Model):
    """
    A room group represents some rooms that have the same semantic
    meaning (the toilets of all buildings for instance).

    Eventually, we shall be able to handle some kind of plan, which is
    for now a simple image loaded from the server.
    """

    name = models.CharField("Name of the group", max_length=100)

    description = models.TextField("Complete description of the group.")

    # plan_path = models.FileField() # To be further discussed.

    GROUP_TYPE = (
        (0, "Floor"),
        (1, "Rooms belonging to an entity"),
        (2, "Rooms for an event")
        )

    group_type = models.SmallIntegerField("Type of group", choices=GROUP_TYPE)

    rooms = models.ManyToManyField(Room, verbose_name="Rooms")

    def __unicode__(self):
        return self.name
