# coding: utf-8

from django.shortcuts import render_to_response, get_object_or_404

from associations.models import Association
from events.models import Event
from rooms.models import Buildind, Room, RoomGroup

def display_all(request):
    """
    Displays public infos about all the rooms.
    """
    
    rooms = Room.objects.all()

    return render_to_response(
        "rooms.html",
        {
            'request': request,
            'rooms': rooms
        })
    

def display_building(request, building_name):
    """
    Displays public informations about a building.

    :param request: request object
    :param building_name: two letters label for the building
    """

    building = get_object_or_404(Buildind, abbreviation=building_name)

    # Rooms in the selected building
    rooms = Room.objects.filter(building=building).order_by('floor', 'name')

    return render_to_response(
        "building.html",
        {
            'request': request,
            'building': building,
            'rooms': rooms,
        })


def display_room(request, building_name, room_number):
    """
    Display public informations about a room.

    This view has a user-friendly url to find a room in the base. For instance,
    the room 106 in the Cauchy (CY) building can be found with "/CY/106/".

    :param request: request object
    :param building_name: two letters label for the building in which the room
    is located
    :param room_number: number of the room
    """

    building = get_object_or_404(Buildind, abbreviation=building_name)
    room = get_object_or_404(Room, name=room_number, building=building)

    associations = Association.objects.filter(room=room)
    events = Event.objects.filter(room=room)

    return render_to_response(
        "fiche_local.html",
        {
            'request': request,
            'local': room,
            'associations': associations,
            'events': events
        })


def display_roomgroup(request, group_name):
    """
    Displays public informations about a room group.

    :param request: request object
    :param group_name: name of the group
    """

    group = get_object_or_404(RoomGroup, name=group_name)

    desc = group.description
    rooms = group.rooms.all()

    return render_to_response(
        "room_group.html",
        {
            'request': request,
            'desc': desc,
            'rooms': rooms
        })
