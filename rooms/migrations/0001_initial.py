# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Buildind'
        db.create_table(u'rooms_buildind', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('abbreviation', self.gf('django.db.models.fields.CharField')(max_length=5)),
            ('campus', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('nb_floors', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'rooms', ['Buildind'])

        # Adding model 'Room'
        db.create_table(u'rooms_room', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('building', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rooms.Buildind'])),
            ('floor', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('nb_seats', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('has_projector', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_amphitheatre', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_control_room', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_electric_plugd', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('has_network_plugs', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('unavailable', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('comments', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'rooms', ['Room'])

        # Adding model 'RoomGroup'
        db.create_table(u'rooms_roomgroup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('group_type', self.gf('django.db.models.fields.SmallIntegerField')()),
        ))
        db.send_create_signal(u'rooms', ['RoomGroup'])

        # Adding M2M table for field rooms on 'RoomGroup'
        m2m_table_name = db.shorten_name(u'rooms_roomgroup_rooms')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('roomgroup', models.ForeignKey(orm[u'rooms.roomgroup'], null=False)),
            ('room', models.ForeignKey(orm[u'rooms.room'], null=False))
        ))
        db.create_unique(m2m_table_name, ['roomgroup_id', 'room_id'])


    def backwards(self, orm):
        # Deleting model 'Buildind'
        db.delete_table(u'rooms_buildind')

        # Deleting model 'Room'
        db.delete_table(u'rooms_room')

        # Deleting model 'RoomGroup'
        db.delete_table(u'rooms_roomgroup')

        # Removing M2M table for field rooms on 'RoomGroup'
        db.delete_table(db.shorten_name(u'rooms_roomgroup_rooms'))


    models = {
        u'rooms.buildind': {
            'Meta': {'object_name': 'Buildind'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'campus': ('django.db.models.fields.SmallIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nb_floors': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'rooms.room': {
            'Meta': {'object_name': 'Room'},
            'building': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rooms.Buildind']"}),
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'floor': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'has_control_room': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_electric_plugd': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'has_network_plugs': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'has_projector': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_amphitheatre': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'nb_seats': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'unavailable': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'rooms.roomgroup': {
            'Meta': {'object_name': 'RoomGroup'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'group_type': ('django.db.models.fields.SmallIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rooms': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['rooms.Room']", 'symmetrical': 'False'})
        }
    }

    complete_apps = ['rooms']