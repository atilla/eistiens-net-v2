# coding: utf-8
from django.contrib import admin
from rooms.models import Buildind, Room, RoomGroup

admin.site.register(Buildind)
admin.site.register(Room)
admin.site.register(RoomGroup)
