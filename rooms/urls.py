from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',

    url(r'^$', 'rooms.views.display_all'),

    url(r'^(?P<building_name>\w+)/$', 'rooms.views.display_building'),

    url(r'^(?P<building_name>\w+)/(?P<room_number>\w+)/$',
        'rooms.views.display_room'),

    url(r'^(?P<group_name>\w+)/$', 'rooms.views.display_roomgroup')
)
