#coding: utf-8

from django import template

import re


VARIABLE_RE = re.compile(r'\{\{ ?(?P<value>.*) ?\}\}')
FORM_RE = re.compile(r'<(input|select|textarea)', re.IGNORECASE)


register = template.Library()


def match_variable(context, variable):
    '''
    Replaces a {{ XXX }} by its content in the current context.
    '''
    n = len(variable)

    if n == 0:
        return '', False
    elif n == 1:
        if hasattr(context, variable[0]):
            return getattr(context, variable[0]), True
        elif hasattr(context, 'get'):
            return context.get(variable[0], ''), variable[0] in context

        return '', False
    elif hasattr(context, variable[0]):
            return '', False

    return match_variable(context[variable[0]], variable[1:])


@register.simple_tag(takes_context=True)
def process(context, string):
    '''
    Processes a {{ XXX }} to get its real content if it exists
    '''

    start = 0
    match = VARIABLE_RE.search(string[start:])
    while match:
        variable = match.group('value').strip()
        replace, found = match_variable(context, variable.split('.'))
        
        if found:
            string = string[:match.start()+start] + replace + string[match.end()+start:]

        start += match.end()
        match = VARIABLE_RE.search(string[start:])

    return string


@register.filter
def bootstrap(form):
    start = 0
    while True:
        match = FORM_RE.search(form[start:])

        if match is None:
            break

        group = match.group()
        start_ = start + match.start()

        next_chevron = form.find('>', start_)
        next_checkbox = form.find('checkbox', start_)

        # Checkboxes don't have the form-control class
        if next_checkbox == -1 or next_checkbox > next_chevron:
            form = form[:start_] + group +' class="form-control"' + form[start_+len(group):]

        start += match.end()

    return form
