from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',

    url(r'^features$', 'portal.views.features'),
    url(r'^about$', 'portal.views.about'),
    url(r'^$', 'portal.views.home'),
    url(r'^login$', 'portal.views.login'),
    url(r'^logout$', 'portal.views.logout'),
    url(r'^404$', 'portal.views.error_404'),
)
