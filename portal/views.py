#coding: utf-8

from django.shortcuts import render, render_to_response
from django.contrib import auth
from django.http import HttpResponseRedirect
from django.template import RequestContext
from ldap.filter import escape_filter_chars
from datetime import datetime
import tooltip

from associations.models import Association
from portal.models import Announcement, CarouselItem


def features(request):
    """
    The features page presents the most important features of the site.

    This page can be used as some sort of high-level user documentation. Most
    of the content of this page is static (in the template).
    """

    nb_asso_cergy = Association.objects.filter(campus=0).count()
    nb_asso_pau = Association.objects.filter(campus=1).count()

    # tooltip.content(request,
    #     title="""Vous aimez le nouvel <b>eistiens.net</b> ?""",
    #     text="""Faites-le nous savoir en laissant un petit mot.""")
    # tooltip.icon(request, 'thumbs-up')
    # tooltip.button(request, 'Donner son avis »', 'portal.views.features')

    return render_to_response(
        "features.html",
        {
            'request': request,
            'nb_asso_cergy': nb_asso_cergy,
            'nb_asso_pau': nb_asso_pau
        }, context_instance=RequestContext(request))


def about(request):
    """
    Thank you page for everyone who made this project possible. :)

    Most of the content of this page is static (in the template).
    """

    return render_to_response(
        "about.html",
        {
            'request': request
        })


def login(request):
    """
    Login page.

    This view is user as redirection target when an unconnected user queries a
    page that requires to be connected (@login_required decorator).
    """

    login_failed = False

    if request.method == 'POST':
        # LDAP DN special characters are escaped in this arbitrary input
        username = escape_filter_chars(request.POST['username'])
        password = request.POST['inputPassword']
        user = auth.authenticate(username=username, password=password)

        if user is not None and user.is_active:
            auth.login(request, user)

            if 'next' in request.GET:
                return HttpResponseRedirect(request.GET['next'])

            return HttpResponseRedirect("/")

        else:
            login_failed = True

    return render(
        request, "login.html",
        {
            'request': request,
            'login_failed': login_failed,
        })


def logout(request):
    """
    Logs out the user and redirects to the site root.
    """

    auth.logout(request)
    return HttpResponseRedirect("/")


def home(request):
    """
    Website homepage (/).
    """

    announcements = Announcement.objects.all().order_by('-publication_date')
    carousel_items = CarouselItem.objects.filter(
        expiration_date__gt=datetime.today).order_by('priority')[:6]

    tooltip.content(request,
        title="""Bienvenue sur la nouvelle version d'<b>eistiens.net</b>""",
        text="""Après des années d'absence, eistiens.net reprend du service en
                tant que portail dédié à la vie étudiante à l'EISTI. Vous
                pouvez désormais consulter <b>eistiens.net</b> pour connaître
                les prochains <b>événements</b> organisés dans l'école ou
                encore découvrir de nouvelles <b>associations</b>""")
    tooltip.icon(request, 'rocket')
    tooltip.button(request, 'En savoir plus', 'portal.views.features')

    return render_to_response(
        "home2.html",
        {
            'request': request,
            'recent_announcements': announcements,
            'carousel_items': carousel_items,
        }, context_instance=RequestContext(request))


def error_404(request):
    """
    Displays a 404 error (page or ressource not found).
    """

    return render_to_response(
        "404.html",
        {
            'request': request
        })
