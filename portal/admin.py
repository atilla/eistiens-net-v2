# coding: utf-8
from django.contrib import admin
from portal.models import Announcement, CarouselItem


admin.site.register(Announcement)
admin.site.register(CarouselItem)
