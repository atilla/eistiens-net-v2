# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Announcement'
        db.create_table(u'portal_announcement', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('publication_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('category', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('content', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'portal', ['Announcement'])

        # Adding model 'CarouselItem'
        db.create_table(u'portal_carouselitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('main_text', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('button_text', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('button_link', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('priority', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('expiration_date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal(u'portal', ['CarouselItem'])


    def backwards(self, orm):
        # Deleting model 'Announcement'
        db.delete_table(u'portal_announcement')

        # Deleting model 'CarouselItem'
        db.delete_table(u'portal_carouselitem')


    models = {
        u'portal.announcement': {
            'Meta': {'object_name': 'Announcement'},
            'category': ('django.db.models.fields.SmallIntegerField', [], {}),
            'content': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'portal.carouselitem': {
            'Meta': {'object_name': 'CarouselItem'},
            'button_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'button_text': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'expiration_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'main_text': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'priority': ('django.db.models.fields.PositiveIntegerField', [], {})
        }
    }

    complete_apps = ['portal']