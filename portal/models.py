# coding: utf-8
from django.db import models


class Announcement(models.Model):

    title = models.CharField("Titre", max_length=200)
    publication_date = models.DateTimeField(auto_now_add=True)

    CATEGORIES = (
        (0, "Actualité étudiante"),
        (1, "Actualité administration"),
        (2, "Actualité pédagogique"),
        (3, "Actualité informatique"),
        (4, "Alerte"),
        (5, "Autre"),
    )
    category = models.SmallIntegerField("Type d'article", choices=CATEGORIES)

    content = models.TextField("Contenu du message")


class CarouselItem(models.Model):
    """
    Carousel slides that can appear on the homepage.

    Each slide must have at least an image and some text to display. Slides
    also have an expiration date after which it won't be displayed. Priority
    level define the relative priority of slides in the carousel. Slides with
    lower priority value will be displayed first.
    """

    main_text = models.CharField("Item text", max_length=200)

    image = models.ImageField("Item image", upload_to="img/home_carousel")

    button_text = models.CharField(
        "Optionnal button", max_length=100, blank=True, null=True)

    button_link = models.URLField(
        "Link for the optionnal button", blank=True, null=True)

    priority = models.PositiveIntegerField("Priority level")

    expiration_date = models.DateField("Expiration date")
