import json
import urllib2


def query_schedule(username, from_, to_):
    """
    Returns the whole schedule of a given user.

    :param username: the login of the user
    :param: from_: the first day (a datetime object)
    :param to_: the last day (a datetime object)
    """

    from_string = from_.strftime('%d_%-m_%Y')
    to_string = to_.strftime('%d_%-m_%Y')

    schedule = urllib2.urlopen('http://edt.atilla.org/search?'
                               'login=%s&from=%s&to=%s' % (username,
                               from_string, to_string))

    return json.loads(schedule.read())
