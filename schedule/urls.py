from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',

    url(r'^mon_edt$', 'schedule.views.my_schedule'),
    url(r'^edt/$', 'schedule.views.ajax_schedule'),
)
