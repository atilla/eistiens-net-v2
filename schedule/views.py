#-*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse

from schedule.api import query_schedule
from events.models import Event
from events.api import create_event_description

import json
import datetime


@login_required
def my_schedule(request):
    """
    Displays connected user's schedule (week by week or day by day)

    :param request: request object
    """
    
    return render_to_response(
        'my_schedule.html',
        {
            'request': request
        })


@login_required
def ajax_schedule(request):
    """
    Returns a JSON object to feed the FullCalendar via AJAX

    The request takes 'start' and 'end' as GET parameters, under a YYYY-MM-DD
    format.

    :param request: request object
    """

    if request.method != 'GET':
        return HttpResponse(json.dumps(
            {
                "success": 0,
                "error": "Not a GET request"
            }), content_type="application/json")

    try:
        start = request.GET['start']
        end = request.GET['end']
    except:
        return HttpResponse(json.dumps(
            {
                "success": 0,
                "error": "Need start and end in GET"
            }), content_type="application/json")

    # Parse the start and end dates to datetime objects
    start_date = datetime.datetime.strptime(start, '%Y-%m-%d')
    end_date = datetime.datetime.strptime(end, '%Y-%m-%d')

    # Query AREL to get all courses during the given interval
    courses = query_schedule(request.user.username, start_date, end_date)

    # Schedule will contain all events that will feed the calendar
    schedule = []
    for course in courses:
        # Because Arel, that's why
        if course['libelle_court'] is None:
            course['libelle_court'] = ''

        # Get the start and end times, which are under the ISO 8601 format
        start_ = course['debut']
        end_ = course['fin']

        # The title of the to-be-created event (e.g: Algorithmique Procedurale (CT))
        title = course['libelle'] + ' (' + course['libelle_court'] + ')'

        for c in schedule:
            # If there already exists the same event (cc Vie associative, Scolarite)
            if c['title'] == title and c['start'] == start_ and c['end'] == end_:
                c['description'] += '<br><br>' + build_description(course)
                break
        else:
            schedule.append({
                'id': len(schedule),
                'title': title,
                'start': start_,
                'end': end_,
                'description': build_description(course),
                'editable': False
            })

    # Add users events to the schedule
    events = Event.objects.filter(
        participants=request.user,
        day__range=(start_date.date(), end_date.date()))

    for event in events:
        start_ = datetime.datetime.combine(
            event.day, event.start_time).isoformat()
        end_ = datetime.datetime.combine(
            event.day, event.end_time).isoformat()

        schedule.append({
            'id': len(schedule),
            'title': event.title,
            'start': start_,
            'end': end_,
            'description': create_event_description(event),
            'color': 'rgb(102, 28, 53)',
            'editable': False
        })

    return HttpResponse(json.dumps(schedule), content_type="application/json")


def build_description(course):
    """
    Returns the description that will be displayed in the modal when
    clicking on a course.

    :param course: a course dictionary
    """

    start = datetime.datetime.strptime(course['debut'], '%Y-%m-%dT%H:%M:%S')
    end = datetime.datetime.strptime(course['fin'], '%Y-%m-%dT%H:%M:%S')

    desc = 'Horaires: %02dh%02d - %02dh%02d<br>' % (start.hour, start.minute, end.hour, end.minute)
    desc += 'Professeur : %s<br>' % course['professeur']
    desc += 'Salle : %s' % course['salle']

    return desc
